require "rubygems"
require 'yaml'
require 'getoptlong'
require "test/unit"
require 'graphviz'

#Red Black Trees
class Node
  attr_accessor :value, :left, :right, :parent, :color
  def initialize(value=nil, left=nil, right=nil, parent = nil, color = nil)
    @value = value
    @left = left
    @right = right
    @parent = parent
    @color = color
  end
end

class RBTree
  attr_reader :root_node
  def initialize( value = nil)
    @root_node = Node.new(value)
  end

  def leftRotate(x)
    y = x.right
    x.right = y.left
    if !y.left.nil?
      y.left.parent = x
    end
    y.parent = x.parent
    if x.parent.nil?
      @root_node = y
    elsif x == x.parent.left
      x.parent.left = y
    else
      x.parent.right = y
    end
    y.left = x
    x.parent = y
  end

  def rightRotate(x)
    y = x.left
    x.left = y.right
    if !y.right.nil?
      y.right.parent = x
    end
    y.parent = x.parent
    if x.parent.nil?
      @root_node = y
    elsif x == x.parent.left
      x.parent.left = y
    else
      y.parent.right = y
    end
    y.right = x
    x.parent = y
  end

  def fixupAdd(z)
    while !z.parent.nil? and !z.parent.parent.nil? and z.parent.color == 'R'
      if z.parent == z.parent.parent.left
        y = z.parent.parent.right
        if y.color == 'R'
          z.parent.color = 'B'
          y.color = 'B'
          z.parent.parent.color = 'R'
          z = z.parent.parent
        else
          if z == z.parent.right
            z = z.parent
            leftRotate(z)
          end
          z.parent.color = 'B'
          z.parent.parent.color = 'R'
          rightRotate(z.parent.parent)
        end
      else
        y = z.parent.parent.left
        if y.color == 'R'
          z.parent.color = 'B'
          y.color = 'B'
          z.parent.parent.color = 'R'
          z = z.parent.parent
        else
          if z == z.parent.left
            z = z.parent
            rightRotate(z)
          end
          z.parent.color = 'B'
          z.parent.parent.color = 'R'
          leftRotate(z.parent.parent)
        end
      end

    end
    @root_node.color = 'B'
  end

  def add( *args )
    args.each do | arg |
      @root_node = add_single @root_node, arg
      z = search(@root_node, arg)
      fixupAdd(z)
    end
  end

  def add_single( node, value )
    if node.value.nil?
      return Node.new( value, Node.new(nil,nil,nil,nil,'B'), Node.new(nil,nil,nil,nil,'B'), nil, 'R' )
    end
    if node.value > value
      node.left = add_single( node.left , value )
      node.left.parent = node
    elsif node.value < value
      node.right = add_single( node.right , value )
      node.right.parent = node
    end
    return node
  end

  def size
    return size_rec( @root_node, 0 ) - 1
  end

  def size_rec( node , size )
    return 1 if not node
    return size_rec( node.left , size ) + size_rec( node.right , size )
  end

  def levels
    return levels_rec( @root_node , 0 )
  end

  def levels_rec( node , levels )
    return levels if not node
    levels += 1
    left_level = levels_rec( node.left , levels )
    right_level = levels_rec( node.right , levels )
    return (left_level > right_level ? left_level : right_level)
  end

  def search(node,item)
    if node.nil?
      return nil
    elsif item == node.value
      return node
    elsif item < node.value
      return search(node.left, item)
    else
      return search(node.right, item)
    end
  end

  def minimum(node)
    while !node.left.nil?
      node = node.left
    end
    return node
  end

  def maximum(node)
    while !node.right.nil?
      node = node.right
    end
    return node
  end

  def successor(node)
    if !node.right.nil?
      return minimum(node.right)
    end
    y = node.parent
    while !y.nil? and node == y.right
      node = y
      y = y.parent
    end
    return y
  end

  def predecessor(node)
    if !node.left.nil?
      return maximum(node.left)
    end
    y = node.parent
    while !y.nil? and node == y.left
      node = y
      y = y.parent
    end
    return y
  end

  def transplant(original,new)
    if original.parent.nil?
      @root_node = new
    elsif original == original.parent.left
      original.parent.left = new
    else
      original.parent.right = new
    end
    if !new.nil?
      new.parent = original.parent
    end
  end

  def delete (node)
    if node.left.nil?
      transplant(node,node.right)
    elsif node.right.nil?
      transplant(node,node.left)
    else
      y = successor(node)
      if y.parent != node
        transplant(y, y.right)
        y.right = node.right
        y.right.parent = y
      end
      transplant(node,y)
      y.left = node.left
      y.left.parent = y
    end
  end

  def print()
    g = GraphViz.new( :G, :type => :digraph )
    queue = Queue.new()
    graphQue = Queue.new()

    colorscheme = Hash.new
    colorscheme['R']="red"
    colorscheme['B']="black"
    temp = @root_node;
    graph_temp = g.add_nodes("#{temp.value}", :color => colorscheme[temp.color])

    while(!temp.nil?) do
      if(!temp.left.nil?)
        graph_left = g.add_nodes("left: #{temp.left.value}" , :color => colorscheme[temp.left.color])
        g.add_edges(graph_temp,graph_left)
        queue.push(temp.left)
        graphQue.push(graph_left)
      end

      if(!temp.right.nil?)
        graph_right = g.add_nodes("right: #{temp.right.value}" , :color => colorscheme[temp.right.color])
        g.add_edges(graph_temp,graph_right)
        queue.push(temp.right)
        graphQue.push(graph_right)
      end

      if(!queue.empty?)
        temp = queue.pop(true)
        graph_temp = graphQue.pop(true)
      else
        temp = nil
      end
    end

    # Generate output image
    g.output( :png => "hello_world#{@i}.png" )
  end
end

opts = GetoptLong.new(
[ '--inputtree', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputtree'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

class TestList < Test::Unit::TestCase
=begin
    10
  8   15
6   11    17
=end
  def test_add
    tree = RBTree.new()
    tree.add(10,8,6,15,17,11,13,12,19)
    node = tree.root_node
    #    assert_equal( node.left.value , 8)
    #    assert_equal( node.left.left.value , 6)
    #    assert_equal( node.right.value , 15)
    #    assert_equal( node.right.right.value , 17)
    #    assert_equal( node.right.left.value , 11)
  end
end