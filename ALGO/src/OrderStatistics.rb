require "rubygems"
require 'yaml'
require 'getoptlong'

class OrderStatistics
  attr_reader :array
  def initialize(array)
    @array = array
  end

  def selectKth(arr,k)
    newArr = Array.new(arr)
    item = selectKthImpl(newArr,0,newArr.length-1,k)
    puts "Kth Item"
    puts item
  end

  private def selectKthImpl(arr,low,high,k)
    if low == high
      return arr[low]
    end
    r = partition(arr,low,high)
    i = r - low + 1
    if k == i
      return arr[r]
    end
    if k < i
      selectKthImpl(arr, low, r - 1, k)
    else
      selectKthImpl(arr, r+1, high, k-i)
    end
  end

  private def partition(arr,low,high)
    # ************** Include for randomized quickSort ***************
    pivot = rand(low..high)
    arr[low] , arr[pivot] = arr[pivot] , arr[low]
    # ************** End of Optional Code *****************
    x = arr[low]
    left = low
    for i in low+1 .. high
      if arr[i] < x
        left = left + 1
        arr[i] , arr[left] = arr[left] , arr[i]
      end
    end
    arr[low] , arr[left] = arr[left] , arr[low]
    return left
  end

  # Median of Median Method
  def kth_smallest(array, k)
    return select(array, k) if (array.length <= 5)
    medians = []
    left,right = [],[]
    array.each_slice(5) do |array_5|
      medians << select(array_5, (array_5.length/2.0).ceil)
    end
    m = kth_smallest(medians, (medians.length/2.0).ceil)
    array.each { |n| n < m ? left << n : (n > m ? right << n : nil) }
    m_posn = left.length + 1
    if k == m_posn
      return m
    elsif k < m_posn
      return kth_smallest(left, k)
    else
      return kth_smallest(right, k - m_posn)
    end
  end

  private def select(array, k)
    array.sort[k - 1]
  end

end

opts = GetoptLong.new(
[ '--inputarray', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputarray'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

a = [2,8,3,1,4,9,7,6]
b = OrderStatistics.new(a)
b.kth_smallest(a,2)