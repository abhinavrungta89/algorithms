require "rubygems"
require 'yaml'
require 'getoptlong'
require "test/unit"
require 'graphviz'

#Binary Search Trees
class Node
  attr_accessor :value, :left, :right, :parent, :size
  def initialize(value=nil, left=nil, right=nil, parent = nil, size = 0)
    @value = value
    @left = left
    @right = right
    @parent = parent
    @size = size
  end
end

class BTree
  attr_reader :root_node
  def initialize( value = nil)
    if value.nil?
      @root_node = nil
    else
      @root_node = Node.new(value,nil,nil,nil,1)
    end
  end

  def add( *args )
    args.each do | arg |
      @root_node = add_single @root_node, arg
    end
  end

  def add_single( node, value )
    if node.nil?
      return Node.new(value,nil,nil,nil,1)
    end
    if node.value > value
      node.left = add_single( node.left , value )
      node.left.parent = node
    elsif node.value < value
      node.right = add_single( node.right , value )
      node.right.parent = node
    end
    node.size = 1
    if !node.left.nil?
      node.size += node.left.size
    end
    if !node.right.nil?
      node.size += node.right.size
    end
    return node
  end

  def size
    return size_rec( @root_node, 0 ) - 1
  end

  def size_rec( node , size )
    return 1 if not node
    return size_rec( node.left , size ) + size_rec( node.right , size )
  end

  def levels
    return levels_rec( @root_node , 0 )
  end

  def levels_rec( node , levels )
    return levels if not node
    levels += 1
    left_level = levels_rec( node.left , levels )
    right_level = levels_rec( node.right , levels )
    return (left_level > right_level ? left_level : right_level)
  end

  def search(node,item)
    if node.nil?
      return false
    elsif item == node.value
      return true
    elsif item < node.value
      return search(node.left, item)
    else
      return search(node.right, item)
    end
  end

  def minimum(node)
    while !node.left.nil?
      node = node.left
    end
    return node
  end

  def maximum(node)
    while !node.right.nil?
      node = node.right
    end
    return node
  end

  def successor(node)
    if !node.right.nil?
      return minimum(node.right)
    end
    y = node.parent
    while !y.nil? and node == y.right
      node = y
      y = y.parent
    end
    return y
  end

  def predecessor(node)
    if !node.left.nil?
      return maximum(node.left)
    end
    y = node.parent
    while !y.nil? and node == y.left
      node = y
      y = y.parent
    end
    return y
  end

  def transplant(original,new)
    if original.parent.nil?
      @root_node = new
    elsif original == original.parent.left
      original.parent.left = new
    else
      original.parent.right = new
    end
    if !new.nil?
      new.parent = original.parent
    end
  end

  def delete (node)
    if node.left.nil?
      transplant(node,node.right)
    elsif node.right.nil?
      transplant(node,node.left)
    else
      y = successor(node)
      if y.parent != node
        transplant(y, y.right)
        y.right = node.right
        y.right.parent = y
      end
      transplant(node,y)
      y.left = node.left
      y.left.parent = y
    end
  end

  def select(node,k)
    rank = 1
    if !node.left.nil?
      rank += node.left.size
    end
    if k == rank
      return node
    elsif k < rank
      return select(node.left, k)
    else
      return select(node.right, k-rank)
    end
  end

  def rank(node,x)
    rank = 1
    if !node.left.nil?
      rank += node.left.size
    end
    y = x
    while y != node
      if y == y.parent.right
        rank += y.parent.left.size + 1
      end
      y = y.parent
    end
    return rank
  end

  def print()
    g = GraphViz.new( :G, :type => :digraph )
    queue = Queue.new()
    graphQue = Queue.new()

    temp = @root_node;
    graph_temp = g.add_nodes("#{temp.value} / #{temp.size}")

    while(!temp.nil?) do
      if(!temp.left.nil?)
        graph_left = g.add_nodes("#{temp.left.value} / #{temp.left.size}")
        g.add_edges(graph_temp,graph_left)
        queue.push(temp.left)
        graphQue.push(graph_left)
      end

      if(!temp.right.nil?)
        graph_right = g.add_nodes("#{temp.right.value} / #{temp.right.size}")
        g.add_edges(graph_temp,graph_right)
        queue.push(temp.right)
        graphQue.push(graph_right)
      end

      if(!queue.empty?)
        temp = queue.pop(true)
        graph_temp = graphQue.pop(true)
      else
        temp = nil
      end
    end

    # Generate output image
    g.output( :png => "hello_world.png" )
  end
end

opts = GetoptLong.new(
[ '--inputtree', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputtree'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

class TestList < Test::Unit::TestCase
=begin
    10
  8   15
6   11    17
=end
  def test_add
    tree = BTree.new()
    tree.add(10,8,6,15,17,11)
    node = tree.root_node
    #    assert_equal( node.value , 10)
    #    assert_equal( node.left.value , 8)
    #    assert_equal( node.left.left.value , 6)
    #    assert_equal( node.right.value , 15)
    #    assert_equal( node.right.right.value , 17)
    #    assert_equal( node.right.left.value , 11)
    puts tree.rank(node,node)
    tree.print()
  end
end