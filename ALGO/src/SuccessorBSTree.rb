require "rubygems"
require 'yaml'
require 'getoptlong'
require "test/unit"
require 'graphviz'

# Binary Search Tree with Successor Attribute instead of Parent.
class Node
  attr_accessor :value, :left, :right, :succ
  def initialize(value=nil, left=nil, right=nil, succ = nil)
    @value = value
    @left = left
    @right = right
    @succ = succ
  end
end

class AugmentedBTree
  attr_reader :root_node
  def initialize( value = nil)
    if value.nil?
      @root_node = nil
    else
      @root_node = Node.new(value)
    end
  end

  def add( *args )
    args.each do | arg |
      add_single arg
    end
  end

  def add_single( value )
    y = nil
    x = @root_node
    while !x.nil?
      y = x
      if value < x.value
        x = x.left
      else
        x = x.right
      end
    end

    if y.nil?
      @root_node = Node.new( value, nil, nil, nil )
    elsif value < y.value
      z = Node.new( value, nil, nil, nil )
      y.left = z
      tmp = predecessor(@root_node, y)
      if !tmp.nil?
        tmp.succ = z
      end
      z.succ = y
    elsif value > y.value
      z = Node.new( value, nil, nil, nil )
      y.right = z
      tmp = y.succ
      y.succ = z
      z.succ = tmp
    end
  end

  def size
    return size_rec( @root_node, 0 ) - 1
  end

  def size_rec( node , size )
    return 1 if not node
    return size_rec( node.left , size ) + size_rec( node.right , size )
  end

  def levels
    return levels_rec( @root_node , 0 )
  end

  def levels_rec( node , levels )
    return levels if not node
    levels += 1
    left_level = levels_rec( node.left , levels )
    right_level = levels_rec( node.right , levels )
    return (left_level > right_level ? left_level : right_level)
  end

  def search(node,item)
    if node.nil?
      return false
    elsif item == node.value
      return true
    elsif item < node.value
      return search(node.left, item)
    else
      return search(node.right, item)
    end
  end

  def minimum(node)
    while !node.left.nil?
      node = node.left
    end
    return node
  end

  def maximum(node)
    while !node.right.nil?
      node = node.right
    end
    return node
  end

  def parent(node)
    y = nil
    x = @root_node
    while node.value != x.value
      y=x
      if node.value < x.value
        x = x.left
      else
        x = x.right
      end
    end
    return y
  end

  def predecessor(node, y)
    if node.nil? or node.succ == y
      return node
    end
    if !node.succ.nil? and y.value < node.succ.value
      return predecessor(node.left, y)
    else
      return predecessor(node.right, y)
    end
  end

  def transplant(original,new)
    y = parent(original)
    if y.nil?
      @root_node = new
    elsif original == y.left
      y.left = new
    else
      y.right = new
    end
  end

  def delete (node)
    successor = node.succ
    if node.left.nil?
      transplant(node,node.right)
    elsif node.right.nil?
      transplant(node,node.left)
    else
      transplant(successor, successor.right)
      successor.right = node.right
      transplant(node,successor)
      successor.left = node.left
      y = predecessor(@root_node,node)
      if !y.nil?
        y.succ = node.succ
      end
    end
  end

  def print()
    g = GraphViz.new( :G, :type => :digraph )
    queue = Queue.new()
    graphQue = Queue.new()

    temp = @root_node;
    graph_temp = g.add_nodes("#{temp.value}")

    while(!temp.nil?) do
      if(!temp.left.nil?)
        graph_left = g.add_nodes("#{temp.left.value}")
        g.add_edges(graph_temp,graph_left)
        queue.push(temp.left)
        graphQue.push(graph_left)
      end

      if(!temp.right.nil?)
        graph_right = g.add_nodes("#{temp.right.value}")
        g.add_edges(graph_temp,graph_right)
        queue.push(temp.right)
        graphQue.push(graph_right)
      end

      if(!queue.empty?)
        temp = queue.pop(true)
        graph_temp = graphQue.pop(true)
      else
        temp = nil
      end
    end

    # Generate output image
    g.output( :png => "hello_world.png" )
  end
end

opts = GetoptLong.new(
[ '--inputtree', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputtree'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

class TestList < Test::Unit::TestCase
=begin
    10
  8   15
6   11    17
=end
  def test_add
    tree = AugmentedBTree.new()
    tree.add(13,8,6,15,17,11,12,18,10)
    node = tree.root_node
    #    assert_equal( node.value , 10)
    #    assert_equal( node.left.value , 8)
    #    assert_equal( node.left.left.value , 6)
    #    assert_equal( node.right.value , 15)
    #    assert_equal( node.right.right.value , 17)
    #    assert_equal( node.right.left.value , 11)
    tree.delete(node.left.right)
    tree.print()
  end
end