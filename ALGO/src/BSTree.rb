require "rubygems"
require 'yaml'
require 'getoptlong'
require "test/unit"
require 'graphviz'

#Binary Search Trees
class Node
  attr_accessor :value, :left, :right, :parent
  def initialize(value=nil, left=nil, right=nil, parent = nil)
    @value = value
    @left = left
    @right = right
    @parent = parent
  end
end

class BSTree
  attr_reader :root_node
  def initialize( value = nil)
    if value.nil?
      @root_node = nil
    else
      @root_node = Node.new(value)
    end
  end

  def add( *args )
    args.each do | arg |
      @root_node = add_single @root_node, arg
    end
  end

  def add_single( node, value )
    if node.nil?
      return Node.new( value )
    end
    if node.value > value
      node.left = add_single( node.left , value )
      node.left.parent = node
    elsif node.value < value
      node.right = add_single( node.right , value )
      node.right.parent = node
    end
    return node
  end

  def size
    return size_rec( @root_node) - 1
  end

  def size_rec( node )
    return 1 if not node
    return size_rec( node.left ) + size_rec( node.right )
  end

  def levels
    return levels_rec( @root_node , 0 )
  end

  def levels_rec( node , levels )
    return levels if not node
    levels += 1
    left_level = levels_rec( node.left , levels )
    right_level = levels_rec( node.right , levels )
    return (left_level > right_level ? left_level : right_level)
  end

  def height()
    return height_rec(@root_node)
  end

  def height_rec(node)
    return 0 if not node
    return (1 + [height_rec(node.left), height_rec(node.right)].max)
  end

  def depth(node)
    return depth_rec(@root_node,node)
  end

  def depth_rec(root,node)
    return 0 if root
    return 0 if not node
    return (1 + [height_rec(node.left), height_rec(node.right)].max)
  end

  def search(node,item)
    if node.nil?
      return false
    elsif item == node.value
      return true
    elsif item < node.value
      return search(node.left, item)
    else
      return search(node.right, item)
    end
  end

  def minimum(node)
    while !node.left.nil?
      node = node.left
    end
    return node
  end

  def maximum(node)
    while !node.right.nil?
      node = node.right
    end
    return node
  end

  def successor(node)
    if !node.right.nil?
      return minimum(node.right)
    end
    y = node.parent
    while !y.nil? and node == y.right
      node = y
      y = y.parent
    end
    return y
  end

  def predecessor(node)
    if !node.left.nil?
      return maximum(node.left)
    end
    y = node.parent
    while !y.nil? and node == y.left
      node = y
      y = y.parent
    end
    return y
  end

  def transplant(original,new)
    if original.parent.nil?
      @root_node = new
    elsif original == original.parent.left
      original.parent.left = new
    else
      original.parent.right = new
    end
    if !new.nil?
      new.parent = original.parent
    end
  end

  def delete (node)
    if node.left.nil?
      transplant(node,node.right)
    elsif node.right.nil?
      transplant(node,node.left)
    else
      y = successor(node)
      if y.parent != node
        transplant(y, y.right)
        y.right = node.right
        y.right.parent = y
      end
      transplant(node,y)
      y.left = node.left
      y.left.parent = y
    end
  end

  def print()
    g = GraphViz.new( :G, :type => :digraph )
    queue = Queue.new()
    graphQue = Queue.new()

    temp = @root_node;
    graph_temp = g.add_nodes("#{temp.value}")

    while(!temp.nil?) do
      if(!temp.left.nil?)
        graph_left = g.add_nodes("#{temp.left.value}")
        g.add_edges(graph_temp,graph_left)
        queue.push(temp.left)
        graphQue.push(graph_left)
      end

      if(!temp.right.nil?)
        graph_right = g.add_nodes("#{temp.right.value}")
        g.add_edges(graph_temp,graph_right)
        queue.push(temp.right)
        graphQue.push(graph_right)
      end

      if(!queue.empty?)
        temp = queue.pop(true)
        graph_temp = graphQue.pop(true)
      else
        temp = nil
      end
    end

    # Generate output image
    g.output( :png => "hello_world.png" )
  end
end

opts = GetoptLong.new(
[ '--inputtree', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputtree'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

class TestList < Test::Unit::TestCase
=begin
    10
  8   15
6   11    17
=end
  def test_add
    tree = BSTree.new()
    tree.add(10,8,6,15,1,4,3,5)
    print(tree.height)
    node = tree.root_node
    #    assert_equal( node.value , 10)
    #    assert_equal( node.left.value , 8)
    #    assert_equal( node.left.left.value , 6)
    #    assert_equal( node.right.value , 15)
    #    assert_equal( node.right.right.value , 17)
    #    assert_equal( node.right.left.value , 11)
    #tree.print()
  end
end