$LOAD_PATH.unshift(File.expand_path('../../lib', __FILE__))
require "gnuplot"

def Thomaes()
  # See sin_wave.rb first
  Gnuplot.open do |gp|
    Gnuplot::Plot.new( gp ) do |plot|

      plot.terminal "png"
      plot.output File.expand_path("../thomae.gif", __FILE__)

      # see sin_wave.rb
      plot.title  "Thomae's Function"
      plot.ylabel "f(x)"
      plot.xlabel "x"

      x = (0..1).step(0.000001).to_a
      y = x.collect { |v| 1.0/(v.to_r.denominator) }

      plot.data << Gnuplot::DataSet.new( [x, y] ) do |ds|
      end
    end
  end
  puts 'created thomae.gif'
end

def RobinsFinal()
  Gnuplot.open do |gp|
    Gnuplot::Plot.new( gp ) do |plot|

      plot.terminal "png"
      plot.output File.expand_path("../robin.gif", __FILE__)

      plot.title  "Robin's Function"
      plot.ylabel "f(n)"
      plot.xlabel "n"

      x1 = (0..10000).step(1).to_a
      y1 = x1.collect { |v| sumOfDivisors(v) }

      constant = Math.exp(0.5772156649)
      x2 = (0..10000).step(1).to_a
      x2.shift(2)
      y2 = x2.collect { |v| constant * Math.log(Math.log(v)) * v }

      plot.data = [Gnuplot::DataSet.new( [x1, y1] ){ |ds|
        ds.with = "lines"
        ds.title = "sigma(n)"
        },
        Gnuplot::DataSet.new( [x2, y2] ){ |ds|
        ds.with = "lines"
        ds.title = "e^(gamma) n ln(ln(n))"
        }
      ]
    end
  end
  puts 'created robin.gif'
end

def sumOfDivisors(n)
  sum = 0
  root = Math.sqrt(n)
  bound = root.to_i
  1.upto(bound) do |i|
    if (n % i == 0) then
      sum += i + n / i
    end
  end
  if(bound == root)
    sum -= root
  end
  return sum
end

def fourier()
  Gnuplot.open do |gp|
    Gnuplot::Plot.new( gp ) do |plot|
      plot.terminal "png"
      plot.output File.expand_path("../fx.gif", __FILE__)

      plot.title  "F(x) Function"
      plot.ylabel "Real F(x)"
      plot.xlabel "x"
      #plot.zlabel "Img F(x)"

      x1,y1,z1 = normalConvertedFunc()
      x2,y2,z2 = scaledFuncPlot(50)

      plot.data = [Gnuplot::DataSet.new( [x1, y1] ){ |ds|
        ds.with = "line"
        ds.title = "normal"
        },
        Gnuplot::DataSet.new( [x2, y2] ){ |ds|
        ds.with = "line"
        ds.title = "fourier(5)"
        }
      ]
    end
  end
  puts 'created f(x).gif'
end

def normalConvertedFunc()
  pi = Math::PI
  h = 0.0001
  x1 = (-pi..-pi/3).step(h).to_a
  y1 = x1.collect { |v| -2.0 * v }

  x2 = (-pi/3..pi/3).step(h).to_a
  x2.shift
  x2.pop
  y2 = x2.collect { |v| 1 }

  x3 = (pi/3..pi).step(h).to_a
  y3 = x3.collect { |v| 0 }

  x = x1 + x2 + x3
  y = y1 + y2 + y3
  z = x.collect { |v| 0 }

  return x,y,z
end

def scaledFuncPlot(n)
  pi = Math::PI
  a = 1/(2*pi)
  arrReal, arrImg = scaledFuncCoeff(n)

  x = (-pi..pi).step(0.0001).to_a
  y = Array.new
  z = Array.new

  x.each do |v|
    tmpSumReal = 0.0
    tmpSumImg = 0.0
    for j in -n .. n
      basisReal = Math.cos(j*v)
      basisImg = Math.sin(j*v)
      tmpSumReal += a * (arrReal[j+n] * basisReal - arrImg[j+n] * basisImg)
      tmpSumImg += a * (arrImg[j+n] * basisReal + arrReal[j+n] * basisImg)
    end
    y.push(tmpSumReal)
    z.push(tmpSumImg)
  end

  return x,y,z
end

# returns an array of co-efficients in sorted order. size of array is obviously n.
def scaledFuncCoeff(n,h=0.0001)
  pi = Math::PI
  arrReal = Array.new
  arrImg = Array.new

  for j in -n .. n
    sumReal , sumImg = IntegralSum(-pi,-pi/3,j,h,1)
    a , b = IntegralSum(-pi/3,pi/3,j,h,2)
    sumReal += a
    sumImg += b
    # no need to call for third sub-range (pi/3 to pi) as it will always be 0
    arrReal.push(sumReal)
    arrImg.push(sumImg)
  end
  return arrReal, arrImg
end

def IntegralSum(low,high,n,h,partial)
  sumReal = 0.0
  sumImg = 0.0
  intervals = (high - low)/h
  for k in 1 .. intervals
    c = low + h*k
    real = Math.cos(-n*c)
    img = Math.sin(-n*c)
    if(partial == 2)
      sumReal += (real * h)
      sumImg += (img * h)
    elsif(partial == 1)
      sumReal += (-2*c*real * h)
      sumImg += (-2*c*img * h)
    end
  end
  return sumReal, sumImg
end

def normalFunc()
  x1 = (0..0.333333).step(0.000001).to_a
  y1 = x1.collect { |v| -2.0 * v }
  z1 = x1.collect { |v| 1 }

  x2 = (0.333333..0.666666).step(0.000001).to_a
  x2.shift
  x2.pop
  y2 = x2.collect { |v| 1 }
  z2 = x2.collect { |v| 0 }

  x3 = (0.666666..1).step(0.000001).to_a
  y3 = x3.collect { |v| 0 }
  z3 = x3.collect { |v| 1 }

  x = x1 + x2 + x3
  y = y1 + y2 + y3
  z = z1 + z2 + z3

  return x,y,z
end

fourier()