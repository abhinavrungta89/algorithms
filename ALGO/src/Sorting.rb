require "rubygems"
require 'yaml'
require 'getoptlong'

class Sorting
  def initialize()
  end

  def insertionSort(arr)
    newArr = Array.new(arr)
    insertionSortImpl(newArr,newArr.length)
    puts "Sorted Array"
    puts newArr
  end

  private def insertionSortImpl(arr,n)
    for j in 1 .. n-1
      key = arr[j]
      i = j-1
      while (i >= 0 and arr[i]>key) do
        arr[i+1]=arr[i]
        i = i-1
      end
      arr[i+1]=key
    end
  end

  def mergeSort(arr)
    newArr = Array.new(arr)
    mergeSortImpl(newArr,0,newArr.length-1)
    puts "Sorted Array"
    puts newArr
  end

  private def mergeSortImpl(arr, low, high)
    if low<high
      mid = (low+high)/2
      mergeSortImpl(arr,low,mid)
      mergeSortImpl(arr,mid+1,high)
      merge(arr,low,mid,high)
    end
  end

  private def merge(arr,low,mid,high)
    n1 = mid - low + 1
    n2 = high - mid
    leftArr = Array.new(n1+1)
    rightArr = Array.new(n2+1)

    for i in 0 .. n1-1
      leftArr[i] = arr[low + i]
    end
    for j in 0 .. n2-1
      rightArr[j] = arr[mid + j + 1]
    end
    leftArr[n1]=Float::INFINITY
    rightArr[n2]=Float::INFINITY
    i = 0
    j = 0
    for k in low .. high
      if leftArr[i] <= rightArr[j]
        arr[k]=leftArr[i]
        i = i+1
      else
        arr[k]=rightArr[j]
        j = j+1
      end
    end
  end

  def selectionSort(arr)
    newArr = Array.new(arr)
    selectionSortImpl(newArr,newArr.length)
    puts "Sorted Array"
    puts newArr
  end

  private def selectionSortImpl(arr,n)
    for j in 0 .. n-2
      smallest = j
      for i in j+1 .. n-1
        if arr[i]< arr[smallest]
          smallest = i
        end
      end
      arr[j] , arr[smallest] = arr[smallest], arr[j]
    end
  end

  def quickSort(arr)
    newArr = Array.new(arr)
    quickSortImpl(newArr,0,newArr.length-1)
    puts "Sorted Array"
    puts newArr
  end

  private def quickSortImpl(arr,low,high)
    if low < high
      i = partition(arr,low,high)
      quickSortImpl(arr, low, i)
      quickSortImpl(arr, i+1, high)
    end
  end

  private def partition(arr,low,high)
    # ************** Include for randomized quickSort ***************
    pivot = rand(low..high)
    arr[low] , arr[pivot] = arr[pivot] , arr[low]
    # ************** End of Optional Code *****************
    x = arr[low]
    left = low
    for i in low+1 .. high
      if arr[i] < x
        left = left + 1
        arr[i] , arr[left] = arr[left] , arr[i]
      end
    end
    arr[low] , arr[left] = arr[left] , arr[low]
    return left
  end
end

opts = GetoptLong.new(
[ '--inputarray', GetoptLong::OPTIONAL_ARGUMENT ])

opts.each do |opt, arg|
  case opt
  when '--inputarray'
    #$test_nodes = YAML::load(YAML::load_file(arg))
  end
end

a = [2,8,3,1,4,5,7,6]
b = Sorting.new()

puts "Unsorted Array"
puts a
b.insertionSort(a)
b.mergeSort(a)
b.selectionSort(a)
b.quickSort(a)