package ctc.queues;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class MaxInWindow {
	public ArrayList<Integer> slidingMaximum(final List<Integer> a, int b) {
		Deque<Integer> max = new java.util.ArrayDeque<Integer>();
		ArrayList<Integer> maxPerSlide = new ArrayList<Integer>();
		if (b > a.size())
			b = a.size();

		for (int i = 0; i < b; i++) {
			while (!max.isEmpty() && a.get(i) >= a.get(max.getLast()))
				max.removeLast();
			max.addLast(i);
		}
		for (int i = b; i < a.size(); i++) {
			maxPerSlide.add(a.get(max.peek()));
			while (!max.isEmpty() && a.get(i) >= a.get(max.getLast()))
				max.removeLast();
			while (!max.isEmpty() && max.peek() <= i - b)
				max.removeFirst();
			max.addLast(i);
			;
		}
		maxPerSlide.add(a.get(max.peek()));
		return maxPerSlide;
	}
}