package ctc.queues;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.PriorityQueue;

class Node {
	int val;

	public Node(int val) {
		this.val = val;
	}
}

class CompareNode implements Comparator<Integer> {
	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		if (o1 > o2)
			return 1;
		else if (o1 < o2)
			return -1;
		return 0;
	}
}

public class SlidingMax {

	public ArrayList<Integer> slidingMaximum(final List<Integer> a, int b) {
		Deque<Integer> max = new java.util.ArrayDeque<Integer>();
		ArrayList<Integer> maxPerSlide = new ArrayList<Integer>();
		if (b > a.size())
			b = a.size();

		for (int i = 0; i < b; i++) {
			while (!max.isEmpty() && a.get(i) >= a.get(max.getLast()))
				max.removeLast();
			max.addLast(i);
		}

		for (int i = b; i < a.size(); i++) {
			maxPerSlide.add(a.get(max.peek()));
			while (!max.isEmpty() && a.get(i) >= a.get(max.getLast()))
				max.removeLast();
			while (!max.isEmpty() && (i - b) >= max.peek())
				max.removeFirst();
			max.addLast(i);
		}
		maxPerSlide.add(a.get(max.peek()));
		return maxPerSlide;
	}

	public static void main(String[] args) {
	}
}