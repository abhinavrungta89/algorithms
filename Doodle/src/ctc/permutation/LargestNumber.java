package ctc.permutation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// Rearrange array of numbers representing digits of a large number such that the resulting number is the greatest.
public class LargestNumber {
	// DO NOT MODIFY THE LIST
	public String largestNumber(final List<Integer> a) {
		Collections.sort(a, new Compare());
		String number = "";
		boolean notAllZero = false;
		for (Integer item : a) {
			if (item != 0)
				notAllZero = true;
			number += String.valueOf(item);
		}
		if (notAllZero)
			return number;
		else
			return "0";
	}
}

class Compare implements Comparator<Integer> {

	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		String xy = String.valueOf(o1) + String.valueOf(o2);
		String yx = String.valueOf(o2) + String.valueOf(o1);
		if (xy.compareTo(yx) > 0)
			return -1;
		else if (xy.compareTo(yx) < 0)
			return 1;
		else
			return 0;
	}
}