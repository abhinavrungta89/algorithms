package ctc.permutation;

import java.math.BigInteger;
import java.util.ArrayList;

public class GetPermutation {
	ArrayList<Integer> unusedNumbers;

	BigInteger fact(int factorialNo) {
		BigInteger fact = BigInteger.ONE;
		for (int i = 2; i <= factorialNo; i++) {
			fact = fact.multiply(new BigInteger(String.valueOf(i)));
		}
		return fact;
	}

	public String getPermutation(int n, int k) {
		StringBuilder permutation = new StringBuilder(n);
		unusedNumbers = new ArrayList<Integer>();
		int i;
		for (i = 1; i <= n; i++) {
			unusedNumbers.add(i);
		}
		BigInteger multiplier = fact(n);
		BigInteger rank = BigInteger.ONE;
		int len = 0;
		BigInteger bg = new BigInteger(String.valueOf(k));
		while (len < n) {
			// System.out.println("len " + len);
			// System.out.println("Rank " + rank);
			multiplier = multiplier.divide(new BigInteger(String.valueOf(n - len)));
			boolean alreadyAdded = false;
			int prev = 1;
			for (i = 1; i <= n; ++i) {
				int countRight = getCountRight(i);
				// System.out.println("Count " + countRight);
				if (countRight != -1) {
					if (multiplier.multiply(new BigInteger(String.valueOf(countRight))).add(rank).equals(bg)) {
						permutation.append(i);
						// System.out.println("Added --- " + (i));
						unusedNumbers.remove(new Integer(i));
						int j = 0;
						while (j < unusedNumbers.size()) {
							permutation.append(unusedNumbers.get(j));
							j++;
						}
						return permutation.toString();
					} else if (multiplier.multiply(new BigInteger(String.valueOf(countRight))).add(rank)
							.compareTo(bg) == 1) {
						// System.out.println("Added " + prev);
						permutation.append(prev);
						rank = multiplier.multiply(new BigInteger(String.valueOf(getCountRight(prev)))).add(rank);
						unusedNumbers.remove(new Integer(prev));
						alreadyAdded = true;
						break;
					}
					prev = i;
				}
			}
			if (!alreadyAdded) {
				// System.out.println("Added Here " + prev);
				permutation.append(prev);
				rank = multiplier.multiply(new BigInteger(String.valueOf(getCountRight(prev)))).add(rank);
				unusedNumbers.remove(new Integer(prev));
			}
			len++;
		}
		return permutation.toString();
	}

	int getCountRight(int i) {
		return unusedNumbers.indexOf(i);
	}

	public static void main(String[] args) {
		GetPermutation gp = new GetPermutation();
		System.out.println(gp.getPermutation(4, 8));

	}

}
