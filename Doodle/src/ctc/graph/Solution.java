package ctc.graph;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class Solution {

	BigInteger N;
	Set<Integer> stateVisited;
	String number;

	public String multiple(int A) {
		N = new BigInteger(Integer.toString(A));
		stateVisited = new HashSet<Integer>();
		if (1 % A == 0)
			return "1";
		stateVisited.add(1 % A);
		number = "1";
		recur("1");
		return number;
	}

	public int getMod(String number) {
		BigInteger num = new BigInteger(number);
		return num.mod(N).intValue();
	}

	public boolean recur(String currentVal) {
		String num1 = currentVal + "0";
		int mod1 = getMod(num1);
		if (mod1 == 0) {
			String tmp = new String(num1);
			if (number.equals("1"))
				number = tmp;
			if (tmp.length() < number.length())
				number = tmp;
			return true;
		}
		boolean found = false;
		System.out.println("Num1 " + num1 + ", MOD1 " + mod1 + "State Visited");
		System.out.println(stateVisited);
		if (!stateVisited.contains(mod1)) {
			stateVisited.add(mod1);
			found = recur(num1);
			stateVisited.remove(mod1);
		}

		String num2 = currentVal + "1";
		int mod2 = getMod(num2);
		if (mod2 == 0) {
			String tmp = new String(num2);
			if (number.equals("1"))
				number = tmp;
			if (tmp.length() < number.length())
				number = tmp;
			return true;
		}
		System.out.println("Num2 " + num2 + ", MOD2 " + mod2 + "State Visited");
		System.out.println(stateVisited);
		if (!stateVisited.contains(mod2)) {
			stateVisited.add(mod2);
			found = recur(num2) || found; 
			stateVisited.remove(mod2);
		}
		return found;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		s.multiple(55);
	}
}