package ctc.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

// Find number of sub-graphs in a graph.
public class NumberOfConnectedSubGraphs {

	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		String[] arr = stdin.nextLine().split(" ");
		int n = Integer.parseInt(arr[0]);
		int m = Integer.parseInt(arr[1]);
		Map<Integer, ArrayList<Integer>> aList = new HashMap<Integer, ArrayList<Integer>>(n);
		// prep adjacency list.
		for (int i = 0; i < m; i++) {
			arr = stdin.nextLine().split(" ");
			int n1 = Integer.parseInt(arr[0]);
			int n2 = Integer.parseInt(arr[1]);
			if (aList.containsKey(n1)) {
				ArrayList<Integer> tmp = aList.get(n1);
				tmp.add(n2);
				aList.put(n1, tmp);
			} else {
				ArrayList<Integer> tmp = new ArrayList<Integer>();
				tmp.add(n2);
				aList.put(n1, tmp);
			}

			if (aList.containsKey(n2)) {
				ArrayList<Integer> tmp = aList.get(n2);
				tmp.add(n1);
				aList.put(n2, tmp);
			} else {
				ArrayList<Integer> tmp = new ArrayList<Integer>();
				tmp.add(n1);
				aList.put(n2, tmp);
			}
		}

		int k = Integer.parseInt(stdin.nextLine());

		// remove lists for corresponding nodes.
		while (k > 0) {
			int node = Integer.parseInt(stdin.nextLine());
			aList.remove(node);
			k--;
		}
		stdin.close();
		ArrayList<Integer> nodesNotVisited = new ArrayList<Integer>(aList.keySet());
		Queue<Integer> queue = new LinkedList<Integer>();
		int element, count = 0;
		while (!nodesNotVisited.isEmpty()) {
			count++;
			int source = nodesNotVisited.get(0);
			nodesNotVisited.remove(0);
			queue.add(source);
			while (!queue.isEmpty()) {
				element = queue.remove();
				Iterator<Integer> neighbours = aList.get(element).iterator();
				while (neighbours.hasNext()) {
					int neigh = neighbours.next();
					if (aList.containsKey(neigh) && nodesNotVisited.contains(neigh)) {
						queue.add(neigh);
						nodesNotVisited.remove((Object) neigh);
					}
				}
			}
		}
		System.out.println(count);
	}
}
