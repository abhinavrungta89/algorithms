package ctc.array;

import java.util.ArrayList;

/*
 * Get Max Element in a Range in an Array
 */

public class GetMax {
	ArrayList<Integer> tree;

	public GetMax() {
		tree = new ArrayList<Integer>(100);
		for (int i = 0; i < 100; i++) {
			tree.add(i, 0);
		}
	}

	void buildMaxTree(int[] A, int nodeIndex, int a, int b) {
		if (a == b) { // We get to a simple element
			tree.add(nodeIndex, A[a]); // This node stores the only value
		} else {
			int leftChild, rightChild, middle;
			leftChild = 2 * nodeIndex;
			rightChild = 2 * nodeIndex + 1; // Or leftChild+1
			middle = (a + b) / 2;
			buildMaxTree(A, leftChild, a, middle);
			buildMaxTree(A, rightChild, middle + 1, b);
			tree.add(nodeIndex, Math.max(tree.get(leftChild), tree.get(rightChild)));
		}
	}

	int query(int node, int a, int b, int p, int q) {
		if (b < p || a > q) // The actual range is outside this range
			return Integer.MIN_VALUE; // Return a negative big number. Can you
										// figure out why?
		else if (p >= a && b >= q) // Query inside the range
			return tree.get(node);
		int l, r, m;
		l = 2 * node;
		r = l + 1;
		m = (a + b) / 2;
		return Math.max(query(l, a, m, p, q), query(r, m + 1, b, p, q));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = { 23, 17, 9, 45, 78, 2, 4, 6, 90, 1 };
		GetMax gm = new GetMax();
		gm.buildMaxTree(array, 1, 0, array.length - 1);
		System.out.println(gm.query(1, 0, array.length - 1, 2, 6));

	}

}
