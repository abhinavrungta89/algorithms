package ctc.array;

import java.util.Scanner;

public class Solution {
	static int n, k;
	static int[] list;
	static int[] diffIncDec;
	static int sum;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		n = s.nextInt();
		k = s.nextInt();
		list = new int[n];
		diffIncDec = new int[n]; // stores the diff in number of increasing and
									// decreasing sequences from i in the
									// current window.
		for (int i = 0; i < n; i++) {
			list[i] = s.nextInt();
			diffIncDec[i] = 0;
		}

		sum = 0;
		for (int i = 1; i < n; i++) {
			// if current element is greater than prev element, there is an
			// increasing sequence starting at the prev item.
			if (list[i] > list[i - 1]) {
				// if item before prev is also less than that also has one more
				// increasing sequence.
				updateIncSeq(i, 1);
			} else if (list[i] < list[i - 1]) {
				updateDecSeq(i, 1);
			} else {
				int j = 2;
				while (i - j >= 0 && j < k) {
					if (diffIncDec[i - j] < 0) {
						diffIncDec[i - j] -= 1;
						sum -= 1;
						updateDecSeq(i, j + 1);
						break;
					} else if (diffIncDec[i - j] > 0) {
						diffIncDec[i - j] += 1;
						sum += 1;
						updateIncSeq(i, j + 1);
						break;
					}
					j++;
				}
			}
			if (i >= k - 1) {
				if (i >= k) {
					sum -= diffIncDec[i - k];
				}
				System.out.println(sum);
			}
		}
		s.close();
	}

	static void updateIncSeq(int i, int j) {
		while (i - j >= 0 && j < k) {
			if (diffIncDec[i - j] >= 0) {
				diffIncDec[i - j] += 1;
				sum += 1;
			} else
				break;
			j++;
		}
	}

	static void updateDecSeq(int i, int j) {
		while (i - j >= 0 && j < k) {
			if (diffIncDec[i - j] <= 0) {
				diffIncDec[i - j] -= 1;
				sum -= 1;
			} else
				break;
			j++;
		}
	}

}
