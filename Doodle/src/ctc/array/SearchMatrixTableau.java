package ctc.array;

public class SearchMatrixTableau {
	// Search for target when each row is sorted in itself and the first column
	// is sorted.
	public boolean searchMatrix(int[][] matrix, int target) {
		int row = matrix.length;
		int col = matrix[0].length;
		for (int i = 0; i < row; i++) {
			if (target >= matrix[i][0]) {
				if (((i < row - 1) && (target < matrix[i + 1][0])) || i == row - 1) {
					{
						int low = 0;
						int high = col - 1;
						while (low <= high) {
							int mid = (low + high) / 2;
							if (target == matrix[i][mid])
								return true;
							else if (target < matrix[i][mid]) {
								high = mid - 1;
							} else
								low = mid + 1;
						}
						return false;
					}
				}
			}
		}
		return false;
	}

	// Search for target when each row & col is sorted in itself.
	public boolean searchMatrix2(int[][] matrix, int target) {
		int row = matrix.length;
		int col = matrix[0].length;
		int i = row - 1, j = 0;
		while (i >= 0 && j < col) {
			if (matrix[i][j] > target) {
				i--;
			} else if (matrix[i][j] < target) {
				j++;
			} else
				return true;
		}
		return false;
	}

	public static final void main(String[] args) {

	}
}
