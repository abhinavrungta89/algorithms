package ctc.array;

import java.util.ArrayList;

/*
 * Find Max Continous Subarray of only positive numbers.
 */
public class MaxContinousSubArrayOfPositiveNumbers {
	public static void main(String[] args) {

	}

	public static ArrayList<Integer> maxset(ArrayList<Integer> a) {
		int len = a.size();
		int startIndex = 0;
		int tempStartIndex = 0;
		int endIndex = 0;
		long currentSum = 0;
		long maxSum = Long.MIN_VALUE;
		int i;
		for ( i = 0; i < len; i++) {
			if (a.get(i) < 0) {
				if (currentSum > maxSum) {
					startIndex = tempStartIndex;
					endIndex = i - 1;
					maxSum = currentSum;
				} else if (currentSum == maxSum) {
					if ((i - 1 - tempStartIndex) > (endIndex - startIndex)) {
						startIndex = tempStartIndex;
						endIndex = i - 1;
					}
				}
				currentSum = 0;
				tempStartIndex = i + 1;
			} else {
				currentSum += a.get(i);
			}
		}
		if (currentSum > maxSum) {
			startIndex = tempStartIndex;
			endIndex = i - 1;
			maxSum = currentSum;
		} else if (currentSum == maxSum) {
			if ((i - 1 - tempStartIndex) > (endIndex - startIndex)) {
				startIndex = tempStartIndex;
				endIndex = i - 1;
			}
		}
		return new ArrayList<Integer>(a.subList(startIndex, endIndex + 1));
	}
}