package ctc.array;

import java.util.Scanner;

/*
 * Find max continous subarray modulo M
 * O(n^2) solution. Naive Solution.
 */
public class MaxContinousSubArrayNaive {
	public static void main(String[] args) {
		// scan input.
		Scanner stdin = new Scanner(System.in);
		int tests = Integer.parseInt(stdin.nextLine());
		for (int i = 0; i < tests; i++) {
			calc(stdin);
		}
		stdin.close();
	}

	public static void calc(Scanner stdin) {
		String[] arr = stdin.nextLine().split(" ");
		long n = Integer.parseInt(arr[0]);
		long m = Integer.parseInt(arr[1]);
		long[] numbers = new long[(int) (n)];
		long[] sum = new long[(int) n];

		String[] numberString = stdin.nextLine().split(" ");
		for (int i = 0; i < n; i++) {
			numbers[i] = Long.parseLong(numberString[i]);
			if (i != 0)
				sum[i] = numbers[i] + sum[i - 1];
			else
				sum[i] = numbers[i];
		}
		long max = Long.MIN_VALUE;
		for (int i = 0; i < n; i++) {
			for (int j = i; j < n; j++) {
				long a = get(i, j, sum) % m;
				if (a > max)
					max = a;
			}
		}
		System.out.println(max);
	}

	public static long get(int start, int end, long[] sum) {
		long l;
		if (start == 0)
			l = 0;
		else
			l = sum[start - 1];
		return sum[end] - l;
	}
}