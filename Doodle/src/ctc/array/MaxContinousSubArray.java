package ctc.array;

import java.util.List;

/*
 * MaxContinous SubArray Sum incl. negative numbers.
 */
public class MaxContinousSubArray {
	public static void main(String[] args) {

	}

	public int maxSubArray(final List<Integer> a) {
		int max_so_far = Integer.MIN_VALUE, max_ending_here = 0;
		for (int i = 0; i < a.size(); i++) {
			max_ending_here = max_ending_here + a.get(i);
			if (max_so_far < max_ending_here)
				max_so_far = max_ending_here;
			if (max_ending_here < 0)
				max_ending_here = 0;
		}
		return max_so_far;
	}
}