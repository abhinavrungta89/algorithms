package ctc.array;

import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.SortedSet;
import java.util.TreeSet;

// Frog needs to jump through leaves to the other end of the pond. The array at A[i] denotes the position where leaf drops at time t = i;

class FrogJump {
	public int solution(int[] A, int X, int D) {
		// write your code in Java SE 8
		PriorityQueue<Integer> currentDroppedLeaves = new PriorityQueue<Integer>();
		currentDroppedLeaves.offer(X);
		int currentDistance = 0;
		int currentTime = 0;
		for (int i = 0; i < A.length; i++) {
			while (!currentDroppedLeaves.isEmpty()) {
				if (currentDistance + D >= currentDroppedLeaves.peek()) {
					currentDistance = currentDroppedLeaves.remove();
				} else
					break;
			}
			if (currentDroppedLeaves.isEmpty()) {
				return currentTime;
			}
			if (currentDistance + D >= A[i]) {
				currentDistance = A[i];
				currentTime = i;
			} else {
				currentDroppedLeaves.offer(A[i]);
				currentTime = i;
			}
		}
		while (!currentDroppedLeaves.isEmpty()) {
			if (currentDistance + D >= currentDroppedLeaves.peek()) {
				currentDistance = currentDroppedLeaves.remove();
			} else
				break;
		}
		if (currentDroppedLeaves.isEmpty()) {
			return currentTime;
		}
		return -1;
	}

	public int solution2(int[] A, int X, int D) {
		// write your code in Java SE 8
		HashMap<Integer, Integer> dist = new HashMap<Integer, Integer>();
		int currentDistance = 0;
		int currentTime = 0;
		for (int i = 0; i < A.length; i++) {
			dist.put(A[i], i);
		}

		SortedSet<Integer> keys = new TreeSet<Integer>(dist.keySet());
		for (Integer key : keys) {
			System.out.println(key + " " + dist.get(key));
			if (currentTime >= dist.get(key)) {
				if (currentDistance + D >= key) {
					currentDistance = key;
					continue;
				}
			}
			if (currentDistance + D >= X)
				break;
			currentTime = dist.get(key);
			currentDistance = key;
			// do something
		}
		if (currentDistance + D >= X)
			return currentTime;
		return -1;
	}

	public static void main(String[] args) {
		FrogJump fg = new FrogJump();
		int[] A = { 2, 12, 33, 10, 16, 40, 23, 30, 45, 46, 47, 50 };
		System.out.println(fg.solution(A, 50, 8));
	}
}