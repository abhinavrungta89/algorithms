package ctc.array;

import java.util.ArrayList;

public class MaxOnes {
	public ArrayList<Integer> maxone(ArrayList<Integer> a, int b) {
		if (a.size() == 0)
			return null;
		int startIndex = 0;
		int maxStartIndex = 0;
		int remainingFlips = b;
		int maxLen = Integer.MIN_VALUE;
		int count = 0;
		int nextIndex = 0;
		startIndex = getNextOnesIndex(a, startIndex, b);
		for (int i = startIndex; i < a.size(); i++) {
			if (a.get(i) == 1) {
				count++;
			} else {
				if (remainingFlips == b)
					nextIndex = i;
				if (remainingFlips > 0) {
					remainingFlips--;
					count++;
				} else {
					if (count > maxLen) {
						maxStartIndex = startIndex;
						maxLen = count;
					}
					remainingFlips = b;
					startIndex = getNextOnesIndex(a, nextIndex, b);
					count = 0;
					i = startIndex - 1;
				}
			}
		}
		if (remainingFlips > 0) {
			count += Math.min(remainingFlips, startIndex);
			startIndex -= Math.min(remainingFlips, startIndex);
		}
		if (count > maxLen) {
			maxStartIndex = startIndex;
			maxLen = count;
		}
		ArrayList<Integer> arr = new ArrayList<Integer>();
		while (maxLen > 0) {
			arr.add(maxStartIndex);
			maxStartIndex++;
			maxLen--;
		}
		return arr;
	}

	public int getNextOnesIndex(ArrayList<Integer> arr, int startIndex, int flips) {
		int i = startIndex;
		while (i < arr.size()) {
			if (arr.get(i) == 1)
				break;
			i++;
		}
		startIndex = i;
		--i;
		while (i >= 0 && flips > 0 && arr.get(i) == 0) {
			i--;
			flips--;
		}
		return i + 1;
	}
}