package ctc.stacks;

import java.util.ArrayList;
import java.util.Stack;

/*
 * Given a relative path like asd/../asdasd/asdad/./asd/../../ give the absolute simplified path which is /asdasd
 */
public class SimplifyPath {
	Stack<Integer> st = new Stack<Integer>();
	Stack<Integer> min = new Stack<Integer>();

	public void push(int x) {
		st.push(x);
		if (min.isEmpty())
			min.push(x);
		else {
			if (min.peek() > x)
				min.push(x);
			else
				min.push(min.peek());
		}
	}

	public void pop() {
		st.pop();
		min.pop();
	}

	public int top() {
		if (st.isEmpty())
			return -1;
		return st.peek();
	}

	public int getMin() {
		if (min.isEmpty())
			return -1;
		return min.peek();
	}

	public String simplifyPath(String a) {
		Stack<Character> st = new Stack<Character>();
		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == '.') {
				if (i + 1 < a.length()) {
					if (a.charAt(i + 1) == '.') {
						i++;
						st.pop();
						while (!st.isEmpty() && st.peek() != '/')
							st.pop();
					} else
						continue;
				}
			} else if (a.charAt(i) == '/') {
				if (st.isEmpty())
					st.push('/');
				else if (st.peek() != '/')
					st.push('/');
			} else
				st.push(a.charAt(i));
		}
		StringBuilder ch = new StringBuilder();
		while (!st.isEmpty() && st.peek() == '/')
			st.pop();
		while (!st.isEmpty())
			ch.insert(0, st.pop());
		if(ch.length()==0)
			ch.insert(0, '/');
		if (ch.charAt(0) != '/')
			ch.insert(0, '/');
		
		return ch.toString();
	}

}
