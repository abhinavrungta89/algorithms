package ctc.stacks;

import java.util.ArrayList;
import java.util.Stack;

public class MaxRectangleAreaHistograms {

	Stack<Integer> st = new Stack<Integer>();

	public int largestRectangleArea(ArrayList<Integer> a) {
		int maxArea = Integer.MIN_VALUE;
		int rightIndex;
		int leftIndex = -1;
		int current;
		for (int i = 0; i < a.size(); i++) {
			if (st.isEmpty())
				st.push(i);
			else if (a.get(i) >= st.peek())
				st.push(i);
			else if (a.get(i) < st.peek()) {
				rightIndex = i;
				while ((current = a.get(st.peek())) > a.get(i)) {
					leftIndex = -1;
					while (!st.isEmpty() && a.get(st.peek()) == current) {
						st.pop();
					}
					if (!st.isEmpty())
						leftIndex = st.peek();
					int area = current * (rightIndex - leftIndex - 1);
					if (area > maxArea)
						maxArea = area;
				}
				st.push(i);
			}
		}
		rightIndex = a.size();
		while (!st.isEmpty()) {
			current = a.get(st.peek());
			leftIndex = -1;
			while (!st.isEmpty() && a.get(st.peek()) == current) {
				st.pop();
			}
			if (!st.isEmpty())
				leftIndex = st.peek();
			int area = current * (rightIndex - leftIndex - 1);
			if (area > maxArea)
				maxArea = area;
		}
		return maxArea;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
