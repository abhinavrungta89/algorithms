package ctc.stacks;

import java.util.Stack;

public class CheckSyntaxBraces {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int braces(String a) {
		char[] arr = a.toCharArray();
		Stack<Character> st = new Stack<Character>();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == '(' || arr[i] == '+' || arr[i] == '*' || arr[i] == '-' || arr[i] == '/')
				st.push(arr[i]);
			else if (arr[i] == ')') {
				if (!st.isEmpty() && st.peek() == '(') {
					return 1;
				} else {
					while (st.pop() != '(')
						continue;
				}
			} else
				continue;
		}
		return 0;
	}

}
