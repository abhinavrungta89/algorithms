package ctc.stacks;

import java.util.ArrayList;
import java.util.Stack;

/*
 * Implement an iterator for an n-way tree.
 */
class Node {
	int data;
	ArrayList<Node> child;

	public Node(int data) {
		this.data = data;
		child = new ArrayList<Node>();
	}
}

class IteratorItem {
	Node tmp;
	int childNo;

	public IteratorItem(Node tmp, int childNo) {
		this.tmp = tmp;
		this.childNo = childNo;
	}
}

public class NWayTreeIterator {
	Stack<IteratorItem> st;
	Node current;

	public NWayTreeIterator(Node root) {
		st = new Stack<IteratorItem>();
		current = root;
	}

	public boolean hasNext() {
		if (current == null)
			return false;
		return true;
	}

	public Node next() {
		Node next = current;
		if (current.child.size() > 0) {
			st.push(new IteratorItem(current, 0));
			current = current.child.get(0);
		} else {
			while (!st.isEmpty()) {
				IteratorItem it = st.pop();
				Node tmp = it.tmp;
				if (it.childNo < tmp.child.size() - 1) {
					it.childNo += 1;
					st.push(it);
					current = tmp.child.get(it.childNo);
					break;
				}
			}
			if (st.isEmpty())
				current = null;
		}
		return next;
	}

	public static void main(String[] args) {
		Node root = new Node(1);
		root.child.add(new Node(2));
		root.child.add(new Node(3));
		root.child.add(new Node(4));
		root.child.get(0).child.add(new Node(5));
		root.child.get(0).child.add(new Node(6));

		NWayTreeIterator itr = new NWayTreeIterator(root);
		while (itr.hasNext()) {
			System.out.println(itr.next().data);
		}
	}

}
