package ctc.stacks;

import java.util.ArrayList;
import java.util.Stack;

public class EvaluateExpression {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public int evalRPN(ArrayList<String> a) {
		Stack<Integer> st = new Stack<Integer>();
		for (int i = 0; i < a.size(); i++) {
			String item = a.get(i);
			if (item.equals("*") || item.equals("-") || item.equals("+") || item.equals("/")) {
				int op2 = st.pop();
				int op1 = st.pop();
				int val = 0;
				if (item == "*")
					val = op1 * op2;
				else if (item == "+")
					val = op1 + op2;
				else if (item == "/")
					val = op1 / op2;
				else if (item == "-")
					val = op1 - op2;
				st.push(val);
			} else {
				int val = Integer.parseInt(item);
				st.push(val);
			}
		}
		return st.peek();
	}

}
