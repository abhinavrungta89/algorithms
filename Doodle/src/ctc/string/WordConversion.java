package ctc.string;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

// Convert A Word to Another Using One Edit Operation only and given dictionary words.
public class WordConversion {
	public static void main(String[] args) {
		Set<String> dictionary = new HashSet<String>();
		dictionary.add("ABC");
		dictionary.add("ABD");
		dictionary.add("ACD");
		dictionary.add("ACC");
		LinkedList<String> tr = transform("abc", "acd", dictionary);
		System.out.println(Arrays.deepToString(tr.toArray()));
	}

	public static LinkedList<String> transform(String startWord, String stopWord, Set<String> dictionary) {
		startWord = startWord.toUpperCase();
		stopWord = stopWord.toUpperCase();
		Stack<String> actionQueue = new Stack<String>();
		Set<String> visitedSet = new HashSet<String>();
		Map<String, String> backtrackMap = new TreeMap<String, String>();
		actionQueue.add(startWord);
		visitedSet.add(startWord);
		while (!actionQueue.isEmpty()) {
			String w = actionQueue.pop();
			/* For each possible word v from w with one edit operation */
			for (String v : getOneEditWords(w)) {
				if (v.equals(stopWord)) {
					// Found our word! Now, back track.
					LinkedList<String> list = new LinkedList<String>(); // Append
					list.add(v);
					while (w != null) {
						list.add(0, w);
						w = backtrackMap.get(w);
					}
					return list;
				}

				/* If v is a dictionary word */
				if (dictionary.contains(v)) {
					if (!visitedSet.contains(v)) {
						actionQueue.add(v);
						visitedSet.add(v); // mark visited
						backtrackMap.put(v, w);
					}
				}
			}
		}
		return null;
	}

	public static Set<String> getOneEditWords(String word) {
		Set<String> words = new TreeSet<String>();
		for (int i = 0; i < word.length(); i++) {
			char[] wordArray = word.toCharArray(); // change that letter.
			for (char c = 'A'; c <= 'Z'; c++) {
				if (c != word.charAt(i)) {
					wordArray[i] = c;
					words.add(new String(wordArray));
				}
			}
		}
		return words;
	}
}
