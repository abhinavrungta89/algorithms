package ctc.string;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

class LongestChainWordsRemoveOneLetter {
	class SortString implements Comparator<String> {

		public int compare(String o1, String o2) {
			// TODO Auto-generated method stub
			if (o1.length() > o2.length())
				return -1;
			else if (o1.length() > o2.length())
				return 1;
			else
				return 0;
		}
	}

	static HashMap<Integer, ArrayList<Integer>> jumps = new HashMap<Integer, ArrayList<Integer>>();

	static int longest_chain(String[] w) {
		System.out.println("".toCharArray());
		LongestChainWordsRemoveOneLetter s = new LongestChainWordsRemoveOneLetter();
		Arrays.sort(w, s.new SortString());
		for (int i = 0; i < w.length; i++) {
			addJumps(w[i], w, i + 1);
			System.out.println(w[i]);
			System.out.println(jumps.get(i));
		}

		int longestChain = Integer.MIN_VALUE;
		Iterator<Integer> itr = jumps.keySet().iterator();
		while (itr.hasNext()) {
			int key = itr.next();
			if (w[key].length() >= longestChain) {
				int len = recur_longest(key, 0);
				System.out.println(w[key]);
				System.out.println(len);

				if (len > longestChain)
					longestChain = len;
			}
		}
		return longestChain;
	}

	static int recur_longest(int index, int length) {
		if (index == -1)
			return length;
		ArrayList<Integer> nextSteps = jumps.get(index);
		Iterator<Integer> itr = nextSteps.iterator();
		int max = Integer.MIN_VALUE;
		while (itr.hasNext()) {
			int len = recur_longest(itr.next(), length + 1);
			if (len > max)
				max = len;
		}
		return max;
	}

	static void addJumps(String str, String[] w, int start) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.add(-1);
		jumps.put(start - 1, arr);
		for (int i = start; i < w.length; i++) {
			if ((str.length() - w[i].length()) > 1) {
				continue;
			}
			if ((str.length() - w[i].length()) == 0) {
				continue;
			}
			if ((str.length() - w[i].length()) == 1) {
				if (isOneStepAway(str.toCharArray(), w[i].toCharArray())) {
					jumps.get(start - 1).add(i);
				}
			}
		}
	}

	static boolean isOneStepAway(char[] s1, char[] s2) {
		int j = 0, k = 0;
		int mismatch = 0;
		
		while (j < s1.length && k < s2.length) {
			if (s1[j] != s2[k]) {
				if (mismatch == 1)
					return false;
				j++;
				mismatch++;
			} else if (s1[j] == s2[k]) {
				j++;
				k++;
			}
		}
		if (mismatch == 1)
			return j == k + 1;
		else
			return j == k;
	}

	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		// final String fileName = System.getenv("OUTPUT_PATH");
		final String fileName = "a";
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		int res;

		int _w_size = 0;
		_w_size = Integer.parseInt(in.nextLine());
		String[] _w = new String[_w_size];
		String _w_item;
		for (int _w_i = 0; _w_i < _w_size; _w_i++) {
			try {
				_w_item = in.nextLine();
			} catch (Exception e) {
				_w_item = null;
			}
			_w[_w_i] = _w_item;
		}

		res = longest_chain(_w);
		bw.write(String.valueOf(res));
		bw.newLine();

		bw.close();
	}
}