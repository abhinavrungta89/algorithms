package ctc.string;

public class RankOfString {

	int findSmallerInRight(char[] str, int low, int high) {
		int countRight = 0, i;

		for (i = low + 1; i <= high; ++i)
			if (str[i] < str[low])
				++countRight;

		return countRight;
	}

	// A function to find rank of a string in all permutations
	// of characters
	public int findRank(String A) {
		char[] str = A.toCharArray();
		int len = str.length;
		int[] fact = new int[len];
		fact[0] = 1;
		for (int i = 1; i < len; i++)
			fact[i] = (fact[i - 1] * i) % 1000003;
		int rank = 1;
		int countRight;

		int i;
		for (i = 0; i < len; ++i) {
			// count number of chars smaller than str[i]
			// fron str[i+1] to str[len-1]
			countRight = findSmallerInRight(str, i, len - 1);

			rank += countRight * fact[len - i - 1];
			rank %= 1000003;
		}

		return rank;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RankOfString rs = new RankOfString();
		System.out.println(rs.findRank("string"));
	}

}
