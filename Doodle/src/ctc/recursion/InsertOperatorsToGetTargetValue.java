package ctc.recursion;

import java.util.Stack;

// Given a list of digits and a target value, insert operators so that the result is the target value.
public class InsertOperatorsToGetTargetValue {
	static char[] operatorExp = { ' ', '+', '-', '*', '/' };

	public static long evaluate(String expression) {
		char[] charArr = expression.toCharArray();

		Stack<Long> values = new Stack<Long>();
		Stack<Character> ops = new Stack<Character>();

		for (int i = 0; i < charArr.length; i++) {
			if (charArr[i] == ' ')
				continue;

			if (charArr[i] >= '0' && charArr[i] <= '9') {
				StringBuffer sbuf = new StringBuffer();
				// There may be more than one digits in number
				while (i < charArr.length && charArr[i] >= '0' && charArr[i] <= '9')
					sbuf.append(charArr[i++]);
				values.push(Long.parseLong(sbuf.toString()));
				i--;
			}

			// Current token is an operator.
			else if (charArr[i] == '+' || charArr[i] == '-' || charArr[i] == '*' || charArr[i] == '/') {
				while (!ops.empty() && hasPrecedence(charArr[i], ops.peek()))
					values.push(applyOp(ops.pop(), values.pop(), values.pop()));

				ops.push(charArr[i]);
			}
		}

		while (!ops.empty())
			values.push(applyOp(ops.pop(), values.pop(), values.pop()));

		return values.pop();
	}

	public static boolean hasPrecedence(char op1, char op2) {
		if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
			return false;
		else
			return true;
	}

	public static long applyOp(char op, long b, long a) {
		switch (op) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			if (b == 0)
				throw new UnsupportedOperationException("Cannot divide by zero");
			return a / b;
		}
		return 0;
	}

	public static void f(String expression, int target) {
		char[] operators = new char[expression.length() - 1];
		recur(expression, 0, operators, target);
	}

	public static void recur(String expression, int index, char[] operators, int target) {
		if (index == expression.length() - 1) {
			String evalExpr = removeWhiteSpace(expression, operators);
			long val = evaluate(evalExpr);
			// System.out.println(val);
			if (val == target) {
				System.out.println(evalExpr + " = " + target);
			}
			return;
		}
		for (int i = 0; i < 5; i++) {
			operators[index] = operatorExp[i];
			recur(expression, index + 1, operators, target);
		}
	}

	public static String removeWhiteSpace(String expression, char[] operators) {
		char[] charArr = expression.toCharArray();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < expression.length() - 1; i++) {
			s.append(charArr[i]);
			if (operators[i] != ' ')
				s.append(operators[i]);
		}
		s.append(charArr[expression.length() - 1]);
		// System.out.println(s.toString());
		return s.toString();
	}

	public static void main(String[] args) {
		f("314159265358", 27182);
	}
}