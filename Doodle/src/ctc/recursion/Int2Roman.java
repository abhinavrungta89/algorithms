package ctc.recursion;

import java.util.HashMap;

public class Int2Roman {
	public String intToRoman(int a) {
		HashMap<Integer, String> numerals = new HashMap<Integer, String>();
		numerals.put(1, "I");
		numerals.put(5, "V");
		numerals.put(10, "X");
		numerals.put(50, "L");
		numerals.put(100, "C");
		numerals.put(500, "D");
		numerals.put(1000, "M");

		StringBuilder strNumeral = new StringBuilder();
		int divisor = 1000;
		while (divisor > 0) {
			int digit = a / divisor;
			if (digit > 0) {
				if (digit >= 1 && digit <= 3) {
					while (digit > 0) {
						strNumeral.append(numerals.get(divisor));
						digit--;
					}
				} else if (digit == 4) {
					strNumeral.append(numerals.get(divisor));
					strNumeral.append(numerals.get(divisor * 5));
				} else if (digit == 5) {
					strNumeral.append(numerals.get(divisor * 5));
				} else if (digit > 5 && digit <= 8) {
					strNumeral.append(numerals.get(divisor * 5));
					while (digit > 5) {
						strNumeral.append(numerals.get(divisor));
						digit--;
					}
				} else if (digit == 9) {
					strNumeral.append(numerals.get(divisor));
					strNumeral.append(numerals.get(divisor * 10));
				}
			}
			a = a % divisor;
			divisor /= 10;
		}
		return strNumeral.toString();
	}
}
