package ctc.hashing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/*
 * Find missing elements from set
 */
public class FindListOfMissingItemsAmongTwoSets {
	public static void main(String[] args) {
		// scan input.
		Scanner stdin = new Scanner(System.in);
		HashMap<Integer, Integer> missing = new HashMap<Integer, Integer>();
		int listASize = Integer.parseInt(stdin.nextLine());
		String[] numberString = stdin.nextLine().split(" ");
		for (int i = 0; i < listASize; i++) {
			int temp = Integer.parseInt(numberString[i]);
			if (missing.containsKey(temp)) {
				missing.put(temp, missing.get(temp) + 1);
			} else
				missing.put(temp, 1);
		}
		int listBSize = Integer.parseInt(stdin.nextLine());
		numberString = stdin.nextLine().split(" ");
		for (int i = 0; i < listBSize; i++) {
			int temp = Integer.parseInt(numberString[i]);
			if (missing.containsKey(temp)) {
				missing.put(temp, missing.get(temp) - 1);
			} else
				missing.put(temp, -1);
		}
		stdin.close();

		Iterator it = missing.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			it.remove(); // avoids a ConcurrentModificationException
		}
	}
}