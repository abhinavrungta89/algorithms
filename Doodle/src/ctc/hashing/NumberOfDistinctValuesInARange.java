package ctc.hashing;

import java.util.ArrayList;
import java.util.HashMap;

public class NumberOfDistinctValuesInARange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public ArrayList<Integer> dNums(ArrayList<Integer> A, int B) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (B > A.size())
			return result;
		HashMap<Integer, Integer> distinct = new HashMap<Integer, Integer>();
		for (int i = 0; i < B; i++) {
			add(distinct, A.get(i));
		}
		for (int i = B; i < A.size(); i++) {
			result.add(distinct.size());
			remove(distinct, A.get(i - B));
			add(distinct, A.get(i));
		}
		result.add(distinct.size());
		return result;
	}

	public void add(HashMap<Integer, Integer> distinct, int key) {
		if (distinct.containsKey(key)) {
			distinct.put(key, distinct.get(key) + 1);
		} else
			distinct.put(key, 1);

	}

	public void remove(HashMap<Integer, Integer> distinct, int key) {
		if (distinct.containsKey(key)) {
			distinct.put(key, distinct.get(key) - 1);
		}
		if (distinct.get(key) == 0)
			distinct.remove(key);
	}

}
