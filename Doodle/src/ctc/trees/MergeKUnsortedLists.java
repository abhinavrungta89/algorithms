package ctc.trees;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

class ListNode {
	public int val;
	public ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}
}

public class MergeKUnsortedLists {
	public ListNode mergeKLists(ArrayList<ListNode> a) {
		TreeMap<Integer, ArrayList<ListNode>> heap = new TreeMap<Integer, ArrayList<ListNode>>();
		Iterator<ListNode> itr = a.iterator();
		while (itr.hasNext()) {
			ListNode tmp = itr.next();
			if (heap.containsKey(tmp.val))
				heap.get(tmp.val).add(tmp);
			else {
				ArrayList<ListNode> list = new ArrayList<ListNode>();
				list.add(tmp);
				heap.put(tmp.val, list);
			}
		}
		ListNode result = new ListNode(0);
		ListNode listItr = result;
		while (!heap.isEmpty()) {
			int min = heap.firstKey();
			ArrayList<ListNode> list = heap.get(min);
			listItr.next = list.get(0);
			list.remove(0);
			if (list.isEmpty())
				heap.remove(min);
			listItr = listItr.next;
			if (listItr.next != null) {
				ListNode tmp = listItr.next;
				if (heap.containsKey(tmp.val))
					heap.get(tmp.val).add(tmp);
				else {
					ArrayList<ListNode> list2 = new ArrayList<ListNode>();
					list2.add(tmp);
					heap.put(tmp.val, list2);
				}
			}
		}
		result = result.next;
		return result;
	}
}
