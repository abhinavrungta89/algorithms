package ctc.trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

// Get Longest Common Prefix using PrefixTree.
public class prefixTree {
	public String val;
	prefixTree[] child;
	public boolean isLeaf;

	public prefixTree() {
		child = new prefixTree[26];
		Arrays.fill(child, null);
		isLeaf = false;
	}

	public void setLeaf(String val) {
		isLeaf = true;
		this.val = val;
	}
}

class Solution2 {
	ArrayList<String> result = new ArrayList<String>();

	public ArrayList<String> prefix(ArrayList<String> a) {
		prefixTree root = new prefixTree();
		Iterator<String> itr = a.iterator();
		while (itr.hasNext()) {
			String word = itr.next();
			add2Tree(root, word, 0);
		}
		itr = a.iterator();
		while (itr.hasNext()) {
			String word = itr.next();
			result.add(getResult(root, word, ""));
		}
		return result;
	}

	public void add2Tree(prefixTree root, String word, int i) {
		if (root.child[word.charAt(i) - 'a'] == null) {
			root.child[word.charAt(i) - 'a'] = new prefixTree();
			root.child[word.charAt(i) - 'a'].setLeaf(word);
		} else if (root.child[word.charAt(i) - 'a'].isLeaf) {
			String w1 = word;
			String w2 = root.child[word.charAt(i) - 'a'].val;
			root.child[word.charAt(i) - 'a'].isLeaf = false;
			add2Tree(root.child[word.charAt(i) - 'a'], w1, i + 1);
			add2Tree(root.child[word.charAt(i) - 'a'], w2, i + 1);
		} else if (!root.child[word.charAt(i) - 'a'].isLeaf) {
			add2Tree(root.child[word.charAt(i) - 'a'], word, i + 1);
		}
	}

	public String getResult(prefixTree root, String word, String prefix) {
		if (root == null)
			return null;
		if (root.isLeaf && root.val.equalsIgnoreCase(word))
			return prefix;
		for (int i = 0; i < 26; i++) {
			char c = (char) ((int) 'a' + i);
			String res = getResult(root.child[i], word, prefix + c);
			if (res != null)
				return res;
		}
		ArrayList<Integer> l = new ArrayList<Integer>();
		Collections.reverse(l);

		return null;

	}
}
