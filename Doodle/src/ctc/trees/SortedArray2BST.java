package ctc.trees;

// Convert a Sorted Array to a Binary Search tree.
class TreeNode {
	int x;
	TreeNode left, right;

	TreeNode(int x) {
		this.x = x;
		this.left = null;
		this.right = null;
	}
}

public class SortedArray2BST {
	public static TreeNode convertArray2Tree(int[] arr, int start, int end, TreeNode root) {
		if (start <= end) {
			int mid = (start + end) / 2;
			root = new TreeNode(arr[mid]);
			root.left = convertArray2Tree(arr, start, mid - 1, root.left);
			root.right = convertArray2Tree(arr, mid + 1, end, root.right);
			return root;
		}
		return null;
	}

	public static void printTree(TreeNode root) {
		if (root != null) {
			printTree(root.left);
			System.out.println(root.x);
			printTree(root.right);
		}
	}

	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8 };
		TreeNode root = null;
		root = convertArray2Tree(arr, 0, arr.length - 1, root);
		printTree(root);
	}

}
