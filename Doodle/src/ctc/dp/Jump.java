package ctc.dp;

import java.util.ArrayList;

/*
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.
 * Each element in the array represents your maximum jump length at that position.
 * Determine if you are able to reach the last index.
 */
public class Jump {

	public int canJump(ArrayList<Integer> a) {
		int size = a.size();
		boolean[] isReachable = new boolean[size];
		for (int i = 0; i < size; i++)
			isReachable[i] = false;
		for (int j = size - 1; j >= 0; j--) {
			if (j + a.get(j) >= size - 1)
				isReachable[j] = true;
			else {
				for (int k = 1; k <= a.get(j); k++) {
					if (j + k < size) {
						if (isReachable[j + k]) {
							isReachable[j] = true;
							break;
						}
					}
				}
			}
		}
		if (isReachable[0])
			return 1;
		return 0;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
