package ctc.dp;

import java.util.List;

public class MaxProduct {

	public int maxProduct(final List<Integer> a) {

		if (a == null || a.size() == 0)
			return 0;
		Integer[] A = new Integer[a.size()];
		a.toArray(A);

		int maxLocal = A[0];
		int minLocal = A[0];
		int global = A[0];

		for (int i = 1; i < A.length; i++) {
			int temp = maxLocal;
			maxLocal = Math.max(Math.max(A[i] * maxLocal, A[i]), A[i] * minLocal);
			minLocal = Math.min(Math.min(A[i] * temp, A[i]), A[i] * minLocal);
			global = Math.max(global, maxLocal);
		}
		return global;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
