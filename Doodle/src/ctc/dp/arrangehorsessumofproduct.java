package ctc.dp;

import java.util.ArrayList;
import java.util.Set;

/*
 * You are given a sequence of black and white horses, and a set of K stables numbered 1 to K. You have to accommodate the horses into the stables in such a way that the following conditions are satisfied:
 * You fill the horses into the stables preserving the relative order of horses. For instance, you cannot put horse 1 into stable 2 and horse 2 into stable 1. You have to preserve the ordering of the horses.
 * No stable should be empty and no horse should be left unaccommodated.
 * Take the product (number of white horses * number of black horses) for each stable and take the sum of all these products. This value should be the minimum among all possible accommodation arrangements
 */
public class arrangehorsessumofproduct {
	ArrayList<Integer> countArray = new ArrayList<Integer>();
	int max = Integer.MAX_VALUE;
	int size;

	public int arrange(String a, int b) {
		if (a.length() == 0)
			return 0;
		int count = 1;
		char current = a.charAt(0);
		for (int i = 1; i < a.length(); i++) {
			if (a.charAt(i) == current)
				count++;
			else {
				countArray.add(count);
				count = 1;
				current = a.charAt(i);
			}
		}
		countArray.add(count);
		size = countArray.size();

		ArrayList<ArrayList<Integer>> combo = new ArrayList<ArrayList<Integer>>(b);
		for (int i = 0; i < b; i++) {
			combo.add(new ArrayList<Integer>());
		}
		recur(combo, 0, 0);
		return max;
	}

	public void recur(ArrayList<ArrayList<Integer>> combo, int index, int pos) {
		if (index == size && pos == combo.size() - 1) {
			max = Math.min(max, calcSP(combo));
			return;
		}
		if(index == size || pos == combo.size())
		    return;
		
		// include in current list.
		combo.get(pos).add(0, countArray.get(index));
		recur(combo, index + 1, pos);
		combo.get(pos).remove(0);
		if (pos < combo.size() - 1) {
			combo.get(pos + 1).add(0, countArray.get(index));
			recur(combo, index + 1, pos + 1);
			combo.get(pos + 1).remove(0);
		}
	}

	public int calcSP(ArrayList<ArrayList<Integer>> combo) {
		int sum = 0;
		for (int i = 0; i < combo.size(); i++) {
			if (combo.get(i).size() == 1)
				continue;
			int noOfOdds = 0;
			int noOfEvens = 0;
			for (int j = 0; j < combo.get(i).size(); j++)
			{
			    if(j%2==0)
			        noOfOdds += combo.get(i).get(j);
			    else
			        noOfEvens += combo.get(i).get(j);
			}
			sum += (noOfOdds*noOfEvens);
		}
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
