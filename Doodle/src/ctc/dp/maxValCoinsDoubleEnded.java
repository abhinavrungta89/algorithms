package ctc.dp;

import java.util.ArrayList;

/*
 * There are N coins (Assume N is even) in a line. Two players take turns to take a coin from one of the ends of the line until there are no more coins left. The player with the larger amount of money wins. Assume that you go first.
 * Write a program which computes the maximum amount of money you can win.
 */
public class maxValCoinsDoubleEnded {
	int n;
	int[][] max;

	public int maxcoin(ArrayList<Integer> a) {
		n = a.size();
		if (n == 0)
			return 0;
		if (n == 1)
			return a.get(0);
		max = new int[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				max[i][j] = -1;
		return recur(a, 0, n - 1);
	}

	public int recur(ArrayList<Integer> a, int low, int high) {
		if (low == high)
			return a.get(low);
		else if (high == low + 1)
			return Math.max(a.get(low), a.get(high));

		if (low <= high) {
			// my turn
			if (max[low][high] != -1)
				return max[low][high];
			if ((high - low + 1) % 2 == 0) {
				max[low][high] = Math.max(a.get(low) + recur(a, low + 1, high), a.get(high) + recur(a, low, high - 1));
				return max[low][high];
			} else {
				max[low][high] = Math.min(recur(a, low + 1, high), recur(a, low, high - 1));
				return max[low][high];
			}
		}
		return -1;
	}

	public int maxcoin1(ArrayList<Integer> a) {
		n = a.size();
		if (n == 0)
			return 0;
		if (n == 1)
			return a.get(0);
		max = new int[n][n];
		max[0][0] = a.get(0);
		for (int i = 1; i < n; i++) {
			max[i][i] = a.get(i);
			max[i - 1][i] = Math.max(a.get(i - 1), a.get(i));
		}
		for (int i = 2; i < n; i++) {
			for (int x = 0, y = i; y < n; x++, y++) {
				if (y < n) {
					int t1 = a.get(x) + Math.min(max[x + 2][y], max[x + 1][y - 1]);
					int t2 = a.get(y) + Math.min(max[x][y - 2], max[x + 1][y - 1]);
					max[x][y] = Math.max(t1, t2);
				} else
					break;
			}
		}
		return max[0][n - 1];
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}