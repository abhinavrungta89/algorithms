package ctc.dp;

import java.util.List;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 */
public class MaxProfitStocks {

	public int maxProfit(final List<Integer> A) {
		int n;
		int array[];
		n = A.size();
		if (A == null || n <= 1)
			return 0;

		array = new int[n - 1];
		int profit = 0;
		int prev = 0;
		// Calculate difference in porfit on each day.
		for (int i = 1; i < n; i++) {
			array[i - 1] = A.get(i) - A.get(i - 1);
		}

		// Using Telescopic Summation to calculate max profit.
		for (int i = 0; i < n - 1; i++) {
			prev += array[i];
			profit = Math.max(profit, prev);
			if (prev < 0)
				prev = 0;
		}
		return profit;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
