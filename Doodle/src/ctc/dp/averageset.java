package ctc.dp;

import java.util.ArrayList;
import java.util.Collections;

/*
 * Divide the array into two such that avg of both parts is same.
 */
public class averageset {
	ArrayList<Integer> a;
	ArrayList<Integer> leftPart;
	ArrayList<Integer> rightPart;
	ArrayList<ArrayList<Integer>> result;
	int len;
	double sum;

	public ArrayList<ArrayList<Integer>> avgset(ArrayList<Integer> a) {
		this.a = a;
		Collections.sort(this.a);
		leftPart = new ArrayList<Integer>();
		rightPart = new ArrayList<Integer>();
		result = new ArrayList<ArrayList<Integer>>();
		sum = 0.0;
		len = this.a.size();
		for (int item : this.a) {
			sum += item;
		}
		recur(0.0, 0);
		return result;
	}

	public void recur(double leftPartSum, int index) {

		// all elements are added either to left or right;
		if (index == len) {
			if (leftPart.size() > 0 && leftPart.size() <= len / 2) {
				double rightPartSum = sum - leftPartSum;
				if (leftPartSum / leftPart.size() == rightPartSum / rightPart.size()) {
					ArrayList<Integer> nl = new ArrayList<Integer>(leftPart);
					ArrayList<Integer> nr = new ArrayList<Integer>(rightPart);
					if (nl.size() == nr.size()) {
						if (!isLessThan(nl, nr)) {
							swapList(nl, nr);
						}
						System.out.println(nl.toString());
						System.out.println(nr.toString());
					}
					if (result.size() > 0) {
						if (nl.size() < result.get(0).size()) {
							result.remove(0);
							result.remove(0);
							result.add(nl);
							result.add(nr);
						} else if (nl.size() == result.get(0).size()) {
							if (isLessThan(nl, result.get(0))) {
								result.remove(0);
								result.remove(0);
								result.add(nl);
								result.add(nr);
							}
						}
					} else {
						result.add(nl);
						result.add(nr);
					}
				}
			}
			return;
		}
		leftPart.add(a.get(index));
		leftPartSum += a.get(index);
		recur(leftPartSum, index + 1);
		leftPartSum -= a.get(index);
		leftPart.remove(leftPart.size() - 1);
		rightPart.add(a.get(index));
		recur(leftPartSum, index + 1);
		rightPart.remove(rightPart.size() - 1);
	}

	public void swapList(ArrayList<Integer> list1, ArrayList<Integer> list2) {
		ArrayList<Integer> tmpList = new ArrayList<Integer>(list1);
		list1.clear();
		list1.addAll(list2);
		list2.clear();
		list2.addAll(tmpList);
	}

	public boolean isLessThan(ArrayList<Integer> a, ArrayList<Integer> b) {
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > b.get(i)) {
				return false;
			}
		}
		return true;
	}
}