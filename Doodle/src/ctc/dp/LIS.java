package ctc.dp;

import java.util.List;

public class LIS {

	int maxLen = Integer.MIN_VALUE;
	int size;
	List<Integer> a;
	int[] longestSeq;

	public int naiveMethodLIS(final List<Integer> a) {
		this.a = a;
		size = a.size();
		if (size == 0)
			return 0;
		longestSeq = new int[size];

		// recur(0, -1, 0);
		return maxLen;
	}

	public void recur(int i, int prev, int count) {
		if (i == size)
			maxLen = Math.max(count, maxLen);
		else if (i < size) {
			if (a.get(i) > prev)
				recur(i + 1, a.get(i), count + 1);
			recur(i + 1, prev, count);
		}
	}

	public static int increasingSubsequence(int[] seq) {
		int[] L = new int[seq.length];
		L[0] = 1;
		for (int i = 1; i < L.length; i++) {
			int maxn = 0;
			for (int j = 0; j < i; j++) {
				if (seq[j] < seq[i] && L[j] > maxn) {
					maxn = L[j];
				}
			}
			L[i] = maxn + 1;
		}
		int maxi = 0;
		for (int i = 0; i < L.length; i++) {
			if (L[i] > maxi) {
				maxi = L[i];
			}
		}
		return (maxi);
	}
}