package ctc.dp;

import java.util.ArrayList;

/*
 * Given a 2 * N Grid of numbers, choose numbers such that the sum of the numbers
is maximum and no two chosen numbers are adjacent horizontally, vertically or diagonally, and return it.
 */
public class Adjacent {
	ArrayList<Integer> list;
	int size;

	public int adjacent(ArrayList<ArrayList<Integer>> a) {
		if (a.size() == 0)
			return 0;
		size = a.get(0).size();
		list = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++) {
			list.add(Math.max(a.get(0).get(i), a.get(1).get(i)));
		}
		try {
			return Math.max(recur(size - 1, false), recur(size - 1, true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int recur(int pos, boolean include) {
		if (pos >= 0) {
			if (!include) {
				return Math.max(recur(pos - 1, false), recur(pos - 1, true));
			} else {
				return recur(pos - 1, false) + list.get(pos);
			}
		}
		return 0;
	}
	
public int adjacentDP(ArrayList<ArrayList<Integer>> A) {
	    
	    int n;
	    int dp[];
	    ArrayList<Integer> first, second;
	    first = A.get(0);
	    second = A.get(1);
	    
	    n = A.get(0).size();
	    
	    if (n == 0)
	        return 0;
	    
	    dp = new int[n];
	    dp[0] = Math.max(first.get(0), second.get(0));
	    
	    if (n < 2)
	        return dp[0];
	    
	    dp[1] = Math.max(first.get(1), second.get(1));
	    dp[1] = Math.max(dp[0], dp[1]);
	    
	    for (int i = 2; i < n; i++) {
	        dp[i] = Math.max(first.get(i), second.get(i));
	        dp[i] += dp[i - 2];
	        dp[i] = Math.max(dp[i], dp[i - 1]);
	    }
	    
	    return dp[n - 1];
	}
}