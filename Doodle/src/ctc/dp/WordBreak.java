package ctc.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
 * Split a string into all combinations of words such that each word is in teh dictionary.
 */
class TrieNode {
	char c;
	HashMap<Character, TrieNode> children = new HashMap<Character, TrieNode>();
	boolean isLeaf;

	public TrieNode() {
	}

	public TrieNode(char c) {
		this.c = c;
	}
}

class Trie {
	private TrieNode root;

	public Trie() {
		root = new TrieNode();
	}

	// Inserts a word into the trie.
	public void insert(String word) {
		HashMap<Character, TrieNode> children = root.children;

		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);

			TrieNode t;
			if (children.containsKey(c)) {
				t = children.get(c);
			} else {
				t = new TrieNode(c);
				children.put(c, t);
			}

			children = t.children;

			// set leaf node
			if (i == word.length() - 1)
				t.isLeaf = true;
		}
	}

	// Returns if the word is in the trie.
	public boolean search(String word) {
		TrieNode t = searchNode(word);

		if (t != null && t.isLeaf)
			return true;
		else
			return false;
	}

	// Returns if there is any word in the trie
	// that starts with the given prefix.
	public boolean startsWith(String prefix) {
		if (searchNode(prefix) == null)
			return false;
		else
			return true;
	}

	public TrieNode searchNode(String str) {
		Map<Character, TrieNode> children = root.children;
		TrieNode t = null;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (children.containsKey(c)) {
				t = children.get(c);
				children = t.children;
			} else {
				return null;
			}
		}

		return t;
	}
}

public class WordBreak {
	Trie dict;
	int[][] subStrings;
	ArrayList<String> result;

	public ArrayList<String> wordBreak(String a, ArrayList<String> b) {
		result = new ArrayList<String>();
		dict = new Trie();
		for (int i = 0; i < b.size(); i++) {
			dict.insert(b.get(i));
		}

		subStrings = new int[a.length()][a.length()];
		for (int i = 0; i < a.length(); i++)
			Arrays.fill(subStrings[i], 0);

		for (int i = 0; i < a.length(); i++) {
			for (int j = i; j < a.length(); j++) {
				if (dict.search(a.substring(i, j + 1)))
					subStrings[i][j] = 1;
			}
		}

		recur(a, 0, "");
		Collections.sort(result);
		return result;
	}

	public void recur(String a, int start, String newStr) {
		if (start == a.length()) {
			result.add(newStr.trim());
		} else {
			for (int i = start; i < a.length(); i++) {
				if (subStrings[start][i] == 1) {
					String s = newStr.concat(a.substring(start, i + 1)).concat(" ");
					recur(a, i + 1, s);
				}
			}
		}
	}
}