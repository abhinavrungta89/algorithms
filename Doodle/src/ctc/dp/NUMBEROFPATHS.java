package ctc.dp;

import java.util.ArrayList;

/*
 * Given a grid of size m * n, lets assume you are starting at (1,1) and your goal is to reach (m,n). At any instance, if you are on (x,y), you can either go to (x, y + 1) or (x + 1, y).
 * Now consider if some obstacles are added to the grids. How many unique paths would there be?
 * An obstacle and empty space is marked as 1 and 0 respectively in the grid.
 */
public class NUMBEROFPATHS {
	int[][] numberofpaths;
	int m, n;

	public int uniquePathsWithObstacles(ArrayList<ArrayList<Integer>> a) {
		m = a.size();
		n = a.get(0).size();
		numberofpaths = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				numberofpaths[i][j] = 0;
			}
		}
		numberofpaths[0][0] = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0, y = i; y >= 0; j++, y--) {
				if (j < m) {
					if (a.get(j).get(y) == 1)
						numberofpaths[j][y] = 0;
					else {
						if (j - 1 >= 0)
							numberofpaths[j][y] += numberofpaths[j - 1][y];
						if (y - 1 >= 0)
							numberofpaths[j][y] += numberofpaths[j][y - 1];
					}
					// System.out.println(numberofpaths[j][y]);
				}
			}
		}
		for (int i = 1; i < m; i++) {
			for (int j = n - 1, x = i; x < m; j--, x++) {
				if (j >= 0) {
					if (a.get(x).get(j) == 1)
						numberofpaths[x][j] = 0;
					else {
						if (j - 1 >= 0)
							numberofpaths[x][j] += numberofpaths[x][j - 1];
						if (x - 1 >= 0)
							numberofpaths[x][j] += numberofpaths[x - 1][j];
					}
					// System.out.println(numberofpaths[x][j]);
				}
			}
		}
		return numberofpaths[m - 1][n - 1];
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
