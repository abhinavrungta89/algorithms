package ctc.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class maxAreaOfOnes {
	public int maximalRectangle(ArrayList<ArrayList<Integer>> a) {
		int m = a.size();
		int n = a.get(0).size();
		Integer[][] height = new Integer[m][n];

		int maxArea = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (a.get(i).get(j) == 0) {
					height[i][j] = 0;
				} else {
					height[i][j] = i == 0 ? 1 : height[i - 1][j] + 1;
				}
			}
		}

		for (int i = 0; i < m; i++) {
			List<Integer> li = Arrays.asList(height[i]);
			ArrayList<Integer> l = new ArrayList<Integer>(li.size());
			l.addAll(li);
			System.out.println("Current i: " + i);
			int area = largestRectangleArea(l);
			System.out.println("Current Area " + area);
			if (area > maxArea) {
				maxArea = area;
			}
		}

		return maxArea;
	}

	public int largestRectangleArea(ArrayList<Integer> a) {
		Stack<Integer> st = new Stack<Integer>();
		int maxArea = Integer.MIN_VALUE;
		int rightIndex;
		int leftIndex = -1;
		int current;
		for (int i = 0; i < a.size(); i++) {
			if (st.isEmpty())
				st.push(i);
			else if (a.get(i) >= a.get(st.peek()))
				st.push(i);
			else if (a.get(i) < a.get(st.peek())) {
				rightIndex = i;
				while (!st.isEmpty() && (current = a.get(st.peek())) > a.get(i)) {
					leftIndex = -1;
					// while (!st.isEmpty() && a.get(st.peek()) == current) {
					st.pop();
					// }
					if (!st.isEmpty())
						leftIndex = st.peek();
					int area = current * (rightIndex - leftIndex - 1);
					if (area > maxArea)
						maxArea = area;
				}
				st.push(i);
			}
		}
		rightIndex = a.size();
		while (!st.isEmpty()) {
			current = a.get(st.peek());
			leftIndex = -1;
			// while (!st.isEmpty() && a.get(st.peek()) == current) {
			st.pop();
			// }
			if (!st.isEmpty())
				leftIndex = st.peek();
			int area = current * (rightIndex - leftIndex - 1);
			if (area > maxArea)
				maxArea = area;
		}
		return maxArea;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
