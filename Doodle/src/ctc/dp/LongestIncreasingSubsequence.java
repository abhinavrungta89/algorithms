package ctc.dp;

import java.util.ArrayList;
import java.util.Collections;

/*
 * Longest Monotonic Subsequence in 2D
There is a hypothesis floating around that SAT score is a strong indicator of GPA.
Your task is to provide the strongest counter example for this hypothesis.
Given a data set of (sat, gpa) for the final year of a group of students,
devise an algorithm to construct the longest sequence of (sati, gpai) of students with progressively better SAT scores,
and progressively worse gpa’s, i.e. sat1 < sat2 < … < satk and gpa1 > gpa2 > … > gpak ( Assume SAT scores and gpa’s are unique )
 */
class Node implements Comparable<Node> {
	double sat;
	double gpa;

	public Node(double gpa, double sat) {
		this.sat = sat;
		this.gpa = gpa;
	}

	public int compareTo(Node o2) {
		// TODO Auto-generated method stub
		if (this.sat < o2.sat)
			return -1;
		else if (this.sat > o2.sat)
			return 1;
		else
			return 0;
	}
}

public class LongestIncreasingSubsequence {

	static ArrayList<Node> longestDecreasingSubsequence(ArrayList<Node> arr, int n) {
		int maxLength = 1, bestEnd = 0;
		ArrayList<Integer> lengthOfLongestSubsequence = new ArrayList<Integer>(n);
		ArrayList<Integer> previousIndex = new ArrayList<Integer>(n);
		ArrayList<Node> lds = new ArrayList<Node>();
		// initialy length of longest subsequence for array ending at i=0 is 1
		// (the 0th element itself)
		lengthOfLongestSubsequence.add(0, 1);
		previousIndex.add(0, -1);

		for (int i = 1; i < n; i++) {
			// initialize the length of longest subsequence for new element at
			// i;
			lengthOfLongestSubsequence.add(i, 1);
			previousIndex.add(i, -1);

			for (int j = i - 1; j >= 0; j--)
				// check for all longest subsequences before i. If there is one
				// and a[i] can be added to it, we add i;
				if (lengthOfLongestSubsequence.get(j) + 1 > lengthOfLongestSubsequence.get(i)
						&& arr.get(j).gpa > arr.get(i).gpa) {
					lengthOfLongestSubsequence.set(i, lengthOfLongestSubsequence.get(j) + 1);
					previousIndex.set(i, j);
				}

			// if length of subsequence for current index is max, update.
			if (lengthOfLongestSubsequence.get(i) > maxLength) {
				bestEnd = i;
				maxLength = lengthOfLongestSubsequence.get(i);
			}
			System.out.println(bestEnd);
		}
		// recreate the entire list for max subsequence.
		lds.add(0, arr.get(bestEnd));
		while (previousIndex.get(bestEnd) != -1) {
			bestEnd = previousIndex.get(bestEnd);
			lds.add(0, arr.get(bestEnd));
		}
		return lds;
	}

	public static void main(String[] args) {
		ArrayList<Node> score = new ArrayList<Node>();
		score.add(new Node(101, 2.3));
		score.add(new Node(200, 3.3));
		score.add(new Node(210, 3.4));
		score.add(new Node(102, 4.0));
		score.add(new Node(300, 2.0));

		// sort the array list on sat score in ascending order.
		// Collections.sort(score);
		ArrayList<Node> order = longestDecreasingSubsequence(score, score.size());
		for (int i = 0; i < order.size(); i++)
			System.out.println(order.get(i).sat + "  " + order.get(i).gpa);
	}
}
