package ctc.dp;

import java.util.List;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 */
public class MaxProfitStocksSellTwice {

	public int maxProfit(final List<Integer> A) {
		int n = A.size();
		if (n <= 1)
			return 0;
		int[] array = new int[n - 1];
		int[] left = new int[n];
		int[] right = new int[n];

		// Calculate difference in porfit on each day.
		for (int i = 1; i < n; i++) {
			array[i - 1] = A.get(i) - A.get(i - 1);
		}
		int profit = 0;
		int prev = 0;
		left[0] = 0;
		// Using Telescopic Summation to calculate max profit.
		for (int i = 0; i < n - 1; i++) {
			prev += array[i];
			profit = Math.max(profit, prev);
			left[i + 1] = profit;
			if (prev < 0)
				prev = 0;
		}
		profit = 0;
		prev = 0;
		right[n - 1] = 0;
		// Using Telescopic Summation to calculate max profit.
		for (int i = n - 2; i >= 0; i--) {
			prev += array[i];
			profit = Math.max(profit, prev);
			right[i] = profit;
			if (prev < 0)
				prev = 0;
		}

		profit = 0;
		for (int i = 0; i < n; i++) {
			profit = Math.max(profit, left[i] + right[i]);
		}
		return profit;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
