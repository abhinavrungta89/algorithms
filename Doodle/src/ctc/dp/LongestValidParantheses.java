package ctc.dp;

import java.util.Stack;

public class LongestValidParantheses {

	public int longestValidParentheses(String a) {
		int maxLen = 0;
		int currentLen = 0;
		Stack<Character> s = new Stack<Character>();
		for (int i = 0; i < a.length(); i++) {
			char c = a.charAt(i);
			if (c == '(') {
				s.push(c);
				currentLen++;
			} else if (c == ')') {
				if (!s.isEmpty()) {
					currentLen++;
					s.pop();
				} else {
					maxLen = Math.max(maxLen, currentLen);
					currentLen = 0;
				}
			}
		}
		maxLen = Math.max(maxLen, currentLen);
		return maxLen;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
