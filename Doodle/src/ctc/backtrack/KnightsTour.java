package ctc.backtrack;

public class KnightsTour {
	static boolean chessboard[][] = new boolean[8][8];
	final static int directionsX[] = { -2, -2, 2, 2, -1, -1, 1, 1 };
	final static int directionsY[] = { 1, -1, 1, -1, 2, -2, 2, -2 };

	public static void init() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				chessboard[i][j] = false;
			}
		}
	}

	public static boolean isSafe(int newX, int newY) {
		if (newX >= 0 && newX < 8 && newY >= 0 && newY < 8)
			if (!chessboard[newX][newY])
				return true;
		return false;
	}

	public static boolean KT(int originalX, int originalY, int moves) {
		System.out.println(moves);
		if (moves == 64)
			return true;
		for (int i = 0; i < 8; i++) {
			int newX = originalX + directionsX[i];
			int newY = originalY + directionsY[i];
			if (isSafe(newX, newY)) {
				chessboard[newX][newY] = true;
				if (KT(newX, newY, moves + 1)) {
					return true;
				} else {
					chessboard[newX][newY] = false;
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		KnightsTour.init();
		KnightsTour.chessboard[0][0] = true;
		System.out.println(KnightsTour.KT(0, 0, 1));
	}

}
