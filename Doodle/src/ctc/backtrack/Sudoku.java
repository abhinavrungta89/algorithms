package ctc.backtrack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class Point {
	int x, y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o) {
		Point p = (Point) o;
		if (this.x == p.x && this.y == p.y)
			return true;
		else
			return false;
	}
}

public class Sudoku {

	public void solveSudoku(ArrayList<ArrayList<Character>> a) {
		recurSolve(a, 0);
	}

	public boolean recurSolve(ArrayList<ArrayList<Character>> a, int cellNo) {
		if (cellNo == 81)
			return true;

		int row = cellNo / 9;
		int col = cellNo % 9;

		if (a.get(row).get(col).equals(Character.valueOf('.'))) {
			for (int i = 1; i <= 9; i++) {
				if (isSafe(a, i, row, col)) {
					a.get(row).set(col, Character.valueOf((char) i));
					if (recurSolve(a, cellNo + 1)) {
						return true;
					}
					a.get(row).set(col, Character.valueOf('.'));
				}
			}
		} else {
			return recurSolve(a, cellNo + 1);
		}
		return true;
	}

	public boolean isSafe(ArrayList<ArrayList<Character>> a, int i, int row, int col) {
		int rowGroup = row / 3;
		int colGroup = col / 3;
		for (int x = rowGroup * 3; x < rowGroup * 3 + 3; x++) {
			for (int y = colGroup * 3; y < colGroup * 3 + 3; y++) {
				if (a.get(x).get(y).equals(Character.valueOf((char) i))) {
					return false;
				}
			}
		}
		ArrayList<Character> tmp = a.get(row);
		for (int x = 0; x < 9; x++) {
			if (tmp.get(x).equals(Character.valueOf((char) i))) {
				return false;
			}
		}
		for (int x = 0; x < 9; x++) {
			if (a.get(x).get(col).equals(Character.valueOf((char) i))) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {

	}

	public ArrayList<ArrayList<Integer>> fourSum(ArrayList<Integer> a, int b) {
		ArrayList<ArrayList<Integer>> sol = new ArrayList<ArrayList<Integer>>();
		HashSet<ArrayList<Integer>> set = new HashSet<ArrayList<Integer>>();
		Collections.sort(a);
		for (int i = 0; i < a.size() - 3; i++) {
			for (int j = i + 1; j < a.size() - 2; j++) {
				for (int k = j + 1; k < a.size() - 1; k++) {
					for (int l = k + 1; l < a.size(); l++) {
						if (a.get(i) + a.get(j) + a.get(k) + a.get(l) == b) {
							ArrayList<Integer> item = new ArrayList<Integer>();
							item.add(a.get(i));
							item.add(a.get(j));
							item.add(a.get(k));
							item.add(a.get(l));
							if (!set.contains(item)) {
								sol.add(item);
								set.add(item);
							}
						}
					}
				}
			}
		}
		return sol;
	}

	public int maxPoints(ArrayList<Integer> a, ArrayList<Integer> b) {
		if (a.size() == 0 || a.size() == 1)
			return a.size();
		HashMap<Point, Integer> uniquePts = new HashMap<Point, Integer>();
		for (int i = 0; i < a.size(); i++) {
			Point p = new Point(a.get(i), b.get(i));
			if (uniquePts.containsKey(p)) {
				uniquePts.put(p, uniquePts.get(p) + 1);
			} else {
				uniquePts.put(p, 1);
			}
		}
		if (uniquePts.size() == 1) {
			return a.size();
		}
		HashMap<Double, Set<Point>> count = new HashMap<Double, Set<Point>>();
		Point[] arr = new Point[uniquePts.size()];
		uniquePts.keySet().toArray(arr);
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				double slope = (1.0 * (arr[j].y - arr[i].y)) / (1.0 * (arr[j].x - arr[i].x));
				if (slope == Double.NEGATIVE_INFINITY)
					slope = Double.POSITIVE_INFINITY;
				// System.out.println(slope);
				if (count.containsKey(slope)) {
					Set<Point> tmp = count.get(slope);
					tmp.add(arr[i]);
					tmp.add(arr[j]);
					count.put(slope, tmp);
				} else {
					Set<Point> tmp = new HashSet();
					tmp.add(arr[i]);
					tmp.add(arr[j]);
					count.put(slope, tmp);
				}
			}
		}
		HashMap<Double, Integer> countInt = new HashMap<Double, Integer>();
		Iterator<Double> itr = count.keySet().iterator();
		while (itr.hasNext()) {
			double slope = itr.next();
			Set<Point> pts = count.get(slope);
			Iterator<Point> ptItr = pts.iterator();
			int sum = 0;
			while (ptItr.hasNext()) {
				sum += uniquePts.get(ptItr.next());
			}
			countInt.put(slope, sum);
		}

		return Collections.max(countInt.values());
	}

	public String minWindow(String S, String T) {
		HashMap<Character, Integer> count = new HashMap<Character, Integer>();
		for (int i = 0; i < T.length(); i++) {
			char c = T.charAt(i);
			if (count.containsKey(c)) {
				int size = count.get(c);
				count.put(c, size);
			} else
				count.put(c, 1);
		}
		int windowSize = T.length();
		while (windowSize <= S.length()) {
			HashMap<Character, Integer> window = new HashMap<Character, Integer>();
			for (int i = 0; i < windowSize; i++) {
				if (window.containsKey(S.charAt(i))) {
					window.put(S.charAt(i), window.get(S.charAt(i)) + 1);
				} else
					window.put(S.charAt(i), 1);
			}
			if (match(window, count)) {
				return S.substring(0, windowSize);
			}
			for (int i = 1; i <= S.length() - windowSize; i++) {
				char prev = S.charAt(i - 1);
				char next = S.charAt(i + windowSize - 1);
				int ct_prev = count.get(prev);
				if (ct_prev == 1)
					count.remove(prev);
				else
					count.put(prev, ct_prev - 1);
				if (count.containsKey(next)) {
					count.put(next, count.get(next) + 1);
				} else
					count.put(next, 1);
				if (match(window, count)) {
					return S.substring(i, windowSize);
				}
			}
			windowSize++;
		}
		return "";
	}

	public boolean match(HashMap<Character, Integer> source, HashMap<Character, Integer> target) {
		Iterator<Character> itr = target.keySet().iterator();
		while (itr.hasNext()) {
			char c = itr.next();
			if (!source.containsKey(c)) {
				return false;
			} else {
				if (source.get(c) < target.get(c))
					return false;
			}
		}
		return true;
	}

	public ArrayList<Integer> findSubstring(String a, final List<String> b) {
		ArrayList<Integer> startPos = new ArrayList<Integer>();
		int lengthOfAll = 0;
		Iterator<String> itr = b.iterator();
		while (itr.hasNext()) {
			lengthOfAll += itr.next().length();
		}
		for (int i = 0; i <= a.length() - lengthOfAll; i++) {
			int j = i;
			List<String> tmp = new ArrayList<String>(b.size());
			for(String item: b)
				tmp.add(item);
			for (int k = 0; k < tmp.size(); k++) {
				if (a.startsWith(tmp.get(k), j)) {
					j += tmp.get(k).length();
					tmp.remove(k);
					k = -1;
				}
			}
			if (tmp.size() == 0)
				startPos.add(i);

		}
		return startPos;
	}

}
