package ctc.math;

public class SumOfEvenFibonnaciNumbers {
	public static void fibonnaci() {
		int x = 4000000;
		int sum = 0;
		int a = 1, b = 2;
		for (; b < x;) {
			if (b % 2 == 0) {
				sum += b;
			}
			int c = a + b;
			a = b;
			b = c;
		}
		System.out.println(sum);
	}

	public static void main(String[] args) {
		fibonnaci();
	}
}
