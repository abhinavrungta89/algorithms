package ctc.math;

import java.util.StringTokenizer;

class Solution {
	public static int solution(int[] A) {

		int N = A.length;
		if (N == 0)
			return -1;
		if (N == 1)
			return 0;
		long sum = 0;
		for (int i = 0; i < N; i++)
			sum += A[i];

		long leftSum = 0;
		long rightSum = 0;
		for (int i = 0; i < N; i++) {
			if (i >= 1)
				leftSum += A[i - 1];
			rightSum = sum - leftSum - A[i];
			if (rightSum == leftSum)
				return i;
		}
		return -1;
	}

	public static int solution(String S) {
		StringTokenizer st = new StringTokenizer(S, ".?!");
		int max = 0;
		while (st.hasMoreTokens()) {
			StringTokenizer st2 = new StringTokenizer(st.nextToken());
			if (max < st2.countTokens())
				max = st2.countTokens();
		}
		return max;
	}

	public static void main(String[] args) {
		// int[] A = { 0, -2147483648, -2147483648 };
		// System.out.println(Solution.solution(A));
		// Solution.solution("");
		// write your code in Java SE 8
		int A = -3, B = 17;
		if (A < 0 && B < 0)
			return;
		int upper = (int) Math.floor(Math.sqrt(B));
		System.out.println(upper);
		int lower = 0;
		if (A < 0)
			A = 0;
		else
			lower = (int) Math.ceil(Math.sqrt(A));
		System.out.println(lower);
		// return upper - lower + 1;
	}
}