package ctc.math;

import java.util.ArrayList;
import java.util.Scanner;

public class GetFactors {
	public static void main(String args[]) {
		Scanner s = new Scanner(System.in);
		int n = Integer.parseInt(s.next());
		for (int i = 0; i < n; i++) {
			String fraction[] = s.next().split("/");
			ArrayList<Integer> factor_num = getFactors(Integer.parseInt(fraction[0]));
			ArrayList<Integer> factor_den = getFactors(Integer.parseInt(fraction[1]));

			for (int j = 0; j < factor_den.size(); j++) {
				Integer fact = factor_den.get(j);
				if (factor_num.remove(fact)) {
					factor_den.remove(j);
					j--;
				}
			}

		}
	}

	public static ArrayList<Integer> getFactors(int x) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		int y = 1;
		while (x != 1) {
			if (x % y == 0) {
				x = x / y;
				arr.add(y);
				if (y == 1) {
					y++;
				}
			} else {
				y += 1;
			}
		}
		return arr;
	}
}
