package ads;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

// Class storing edge information in a graph.
class Edge {
	int v1; // index for vertice1.
	int v2; // index for vertice2.
	int wt; // weight of edge.

	public int getV1() {
		return v1;
	}

	public void setV1(int v1) {
		this.v1 = v1;
	}

	public int getV2() {
		return v2;
	}

	public void setV2(int v2) {
		this.v2 = v2;
	}

	public int getWt() {
		return wt;
	}

	public void setWt(int wt) {
		this.wt = wt;
	}

	Edge() {
		v1 = 0;
		v2 = 0;
		wt = 0;
	}

	Edge(int v1, int v2, int wt) {
		this.v1 = v1;
		this.v2 = v2;
		this.wt = wt;
	}
}

class Vertex {
	public int getVerticeNo() {
		return verticeNo;
	}

	public void setVerticeNo(int verticeNo) {
		this.verticeNo = verticeNo;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public Vertex getParent() {
		return parent;
	}

	public void setParent(Vertex parent) {
		this.parent = parent;
	}

	private int verticeNo;
	private int key; // the distance
	private Vertex parent;

}

// Class for calculating Single Source Shortest Path in a Graph.
public class Graph {
	// Adjacency List of the Graph.
	ArrayList<ArrayList<Edge>> adjacencyList = null;
	ArrayList<Vertex> vertexList = null;
	int n = 0, m = 0;

	public static void main(String[] args) throws IOException {
		// to check if all the arguments are provided.
		if (args.length != 3) {
			System.out.println("Incorrect # of Arguments.");
			System.exit(0);
		}
		// get the list of arguments.
		String fileName = args[0];
		int source = Integer.parseInt(args[1]);
		int destination = Integer.parseInt(args[2]);
		Graph g = new Graph();

		g.loadGraph(fileName);
		g.SSP(source);
		g.printPathLength(destination);
		g.printPath(source, destination);
	}

	public int getNoOfNodes() {
		return n;
	}

	public void loadGraph(String fileName) throws IOException {
		// Read the input.
		String line;
		FileInputStream fstream = null;
		BufferedReader br = null;
		try {
			fstream = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		}
		br = new BufferedReader(new InputStreamReader(fstream));
		// read first line and get no of vertices and no of edges.
		line = br.readLine();
		String[] params = line.split(" ");
		n = Integer.parseInt(params[0]);
		m = Integer.parseInt(params[1]);
		// Initialize the adjacency list for n vertices.
		adjacencyList = null;
		adjacencyList = new ArrayList<ArrayList<Edge>>(n);
		for (int i = 0; i < n; i++) {
			ArrayList<Edge> tmp = new ArrayList<Edge>();
			adjacencyList.add(i, tmp);
		}

		// Read the input and populate the adjacency list.
		for (int i = 0; i < m; i++) {
			line = br.readLine();
			if (line.equalsIgnoreCase("")) {
				line = br.readLine();
			}
			params = line.split(" ");
			int v1 = Integer.parseInt(params[0]);
			int v2 = Integer.parseInt(params[1]);
			int wt = Integer.parseInt(params[2]);
			Edge tmp = new Edge(v1, v2, wt);
			// the same edge object is stored in both the lists for v1 and v2.
			adjacencyList.get(v1).add(tmp);
			adjacencyList.get(v2).add(tmp);
		}
		br.close();
	}

	public void printPathLength(int destination) {
		// distance for destination node.
		System.out.println(vertexList.get(destination).getKey());

	}

	private String getPath(int source, int destination) {
		// construct path with parent pointer.
		String path = "";
		int tmp = destination;
		while (tmp != source) {
			path = " " + tmp + path;
			tmp = vertexList.get(tmp).getParent().getVerticeNo();
		}
		path = source + path;
		return path;
	}

	public void printPath(int source, int destination) {
		System.out.println(getPath(source, destination));
	}

	public int getNextHop(int source, int destination) {
		if (source != destination) {
			return Integer.parseInt(getPath(source, destination).split(" ")[1]);
		}
		return -1;
	}

	public void SSP(int source) {
		// Initialize shortestPath and parent array.
		vertexList = null;
		vertexList = new ArrayList<Vertex>(n);
		for (int i = 0; i < n; i++) {
			Vertex v = new Vertex();
			v.setKey(Integer.MAX_VALUE);
			v.setParent(null);
			v.setVerticeNo(i);
			vertexList.add(i, v);
		}

		vertexList.get(source).setKey(0);
		ArrayList<Node> nodeList = new ArrayList<Node>(n);
		FibonnaciHeap fHeap = new FibonnaciHeap();
		for (int i = 0; i < n; i++) {
			Node n = new Node(vertexList.get(i));
			nodeList.add(i, n);
			fHeap.insert(n);
		}
		while (fHeap.getSize() > 0) {
			Vertex node = fHeap.extractMin();
			int u = node.getVerticeNo();
			// relax edges.
			ArrayList<Edge> l = adjacencyList.get(u);
			for (int j = 0; j < l.size(); j++) {
				Edge e = l.get(j);
				// xor of node, v1 and v2. returns v1 if node = v2, else v2 if
				// node = v1
				int v = u ^ e.getV1() ^ e.getV2();
				if (vertexList.get(v).getKey() > vertexList.get(u).getKey()
						+ e.getWt()) {
					fHeap.decreaseKey(nodeList.get(v), vertexList.get(u)
							.getKey() + e.getWt());
					vertexList.get(v).setParent(node);
				}
			}
		}
	}
}