package ads;

class TrieNode {
	public TrieNode(int val) {
		nextHop = val;
		children = new TrieNode[2];
		children[0] = null;
		children[1] = null;
		isLeaf = false;
	}

	public int getValue() {
		return nextHop;
	}

	public void setValue(int val) {
		nextHop = val;
	}

	public void setIsLeaf(boolean val) {
		isLeaf = val;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	private int nextHop;
	TrieNode[] children;
	private boolean isLeaf;
}

// Implements the actual Trie
public class BinaryTrie {
	// Constructor
	public BinaryTrie() {
		root = new TrieNode(-1);
	}

	// Method to insert a new Pair to Trie. Leaf Nodes are at level 32 for IPv4.
	public void insert(Pair ob) {
		int length = ob.key.length();
		TrieNode itr = root;

		// Traverse through all bits of the ip
		for (int i = 0; i < length; i++) {
			char ch = ob.key.charAt(i);
			int bit = ch - '0';
			// If there is already a branch for current bit, get the node.
			if (itr.children[bit] != null)
				itr = itr.children[bit];
			else // Else create a branch.
			{
				TrieNode temp = new TrieNode(-1);
				itr.children[bit] = temp;
				itr = temp;
			}
		}
		// Set isLeaf to true for last bit
		itr.setIsLeaf(true);
		// set the hop in the leaf node for the <ip, hop> pair. IP is not stored
		// as that info is inherent in the path to the leaf node.
		itr.setValue(ob.value);
	}

	public Pair getNextHop(String ip) {
		if (root == null) {
			return null;
		}
		String prefix = "";
		int length = ip.length();
		TrieNode itr = root;

		int i = 0;
		// Traverse through all bits of the ip or until we find a longest
		// matching prefix.
		while (!itr.isLeaf() && i < length) {
			char ch = ip.charAt(i);
			prefix += ch;
			int bit = ch - '0';
			itr = itr.children[bit];
			i++;
		}
		Pair ob = new Pair(prefix, itr.getValue());
		return ob;
	}

	public void trim() {
		postOrder(root);
	}

	public void print() {
		String s = "";
		inorder(root, s);
	}

	private void inorder(TrieNode item, String s) {
		if (item != null) {
			if (item.isLeaf()) {
				System.out.println(s);
				System.out.println(item.getValue());
			}
			if (item.children[0] != null) {
				inorder(item.children[0], s.concat("0"));
			}
			if (item.children[1] != null) {
				inorder(item.children[1], s.concat("1"));
			}
		}
	}

	private int postOrder(TrieNode item) {
		if (item != null) {
			if (item.isLeaf()) {
				return item.getValue();
			} else {
				int leftHop, rightHop;
				leftHop = postOrder(item.children[0]);
				rightHop = postOrder(item.children[1]);
				if (leftHop == rightHop && leftHop != -1) {
					item.setIsLeaf(true);
					item.setValue(leftHop);
					item.children[0] = null;
					item.children[1] = null;
					return item.getValue();
				} else if (leftHop >= 0 && rightHop == -2) {
					item.setIsLeaf(true);
					item.setValue(leftHop);
					item.children[0] = null;
					return item.getValue();
				} else if (rightHop >= 0 && leftHop == -2) {
					item.setIsLeaf(true);
					item.setValue(rightHop);
					item.children[1] = null;
					return item.getValue();
				} else {
					// return -1 if there is a branching in a subtrie and left
					// and right subtrie do not match.
					return -1;
				}
			}
		}
		// return -2 if item is null;
		return -2;
	}

	private TrieNode root;
}