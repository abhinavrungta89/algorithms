package ads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

class Node {
	int degree;
	Node child, next, prev, parent;
	Vertex data;
	boolean marked;

	public Vertex getData() {
		return data;
	}

	public void setData(Vertex data) {
		this.data = data;
	}

	public Node(Vertex d) {
		marked = false;
		next = this;
		prev = this;
		parent = null;
		child = null;
		degree = 0;
		data = d;
	}

	public void print() {
		System.out.println("Data " + data.getVerticeNo());
		System.out.println("Degree " + degree);
		System.out.println("Marked " + marked);
		if (parent != null) {
			System.out.println("Parent " + parent.data.getVerticeNo());
		}

	}
}

public class FibonnaciHeap {
	private Node min;
	private int N; // no of nodes in the heap.

	public FibonnaciHeap() {
		min = null;
		N = 0;
	}

	// decrease key for a Node
	public void decreaseKey(Node item, int d) {
		item.data.setKey(d);
		if ((item.parent != null)
				&& (item.data.getKey() < item.parent.data.getKey())) {
			Node parent = item.parent;
			// Step 1: Remove from Siblings List.
			item = removeFromSibling(item);
			// Step 2: Cascade Cut.
			cascadeCut(parent);
			// Step 3: add item to top level list;
			min = mergeLists(min, item);
			// Step 4: update min if required.
			min = updateMin(min, item);
		}
		if ((item.parent == null) && (item.data.getKey() < min.data.getKey())) {
			min = updateMin(min, item);
		}
	}

	// Remove Arbitary Node.
	public void removeNode(Node item) {
		decreaseKey(item, Integer.MIN_VALUE);
		extractMin();
	}

	// Get value for Min in Heap.
	public Vertex getMin() {
		return min.data;
	}

	// delete min element from heap.
	public Vertex extractMin() {
		Vertex retVal = null;
		if (N > 0) {
			Node tmp = null;
			// if this is not last, store a pointer to the rest of the top list.
			if (min.next != min) {
				tmp = min.next;
			}
			// Remove Min from the top level list.
			min = removeFromSibling(min);
			retVal = min.data;

			if (min.child != null) {
				// make parent as null.
				Node itr = min.child;
				while (itr.next != min.child) {
					itr.parent = null;
					itr = itr.next;
				}
				itr.parent = null;
				// add child to top level list;
				tmp = mergeLists(tmp, min.child);
			}
			// set min pointer to null;
			min = null;
			// if rest of the list present, collapse it to logN number of nodes.
			if (tmp != null) {
				tmp = collapse(tmp);
				min = tmp;
				// update min pointer, by searching entire top level list.
				min = updateMin(min, min.prev);
			}
			// decrease count of Nodes.
			N--;
		}
		return retVal;
	}

	// insert an element in the Fibonacci Heap.
	public void insert(Vertex d) {
		Node item = new Node(d);
		insert(item);
	}

	// insert a node of the Fibonacci Heap.
	public void insert(Node item) {
		min = mergeLists(min, item);
		min = updateMin(min, item);
		N++;
	}

	// Get size of the Heap.
	public int getSize() {
		return N;
	}

	// collapse the list to logN number of trees.
	private Node collapse(Node root) {
		ArrayList<Node> arrList = new ArrayList<Node>();
		// add all the roots of the top level list to Array.
		Node itr = root;
		while (itr.next != root) {
			arrList.add(itr);
			itr = itr.next;
		}
		arrList.add(itr);
		// break all links b/w roots.
		for (Node item : arrList) {
			item.next = item;
			item.prev = item;
		}

		// Temp DataStructure to store tree roots by degree when combining.
		HashMap<Integer, Node> list = new HashMap<Integer, Node>();

		// for all items in Arrlist, do a combine and add to correct index in
		// list[] s.t. there is only one tree with one degree.
		for (Node item : arrList) {
			int d = item.degree;
			while (list.containsKey(d)) {
				Node a = list.get(d);
				list.remove(d);
				if (a.data.getKey() <= item.data.getKey()) {
					a = addChild(a, item);
					item = a;
				} else {
					item = addChild(item, a);
				}
				d++;
			}
			list.put(d, item);
		}

		// take all non empty items from list[] and construct new top list.
		root = null;
		Iterator<Entry<Integer, Node>> it = list.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Node> pair = it.next();
			root = mergeLists(root, pair.getValue());
			it.remove();
		}
		return root;
	}

	// Merge two circular doubly linked lists.
	private Node mergeLists(Node left, Node right) {
		if (left == null && right != null) {
			right.marked = false;
			left = right;
		} else if (left != null && right != null) {
			right.marked = false;
			Node tmp = right.next;
			right.next = left.next;
			right.next.prev = right;
			left.next = tmp;
			left.next.prev = left;
		}
		return left;
	}

	// update Min by comparing to nodes. This technically searches thru all the
	// nodes in the right list. Left is the original Min List. This is dependent
	// on the implementation of mergeList.
	private Node updateMin(Node left, Node right) {
		Node tmp = left;
		Node target = right.next;
		while (tmp.next != target) {
			tmp = tmp.next;
			if (tmp.data.getKey() < left.data.getKey()) {
				left = tmp;
			}
		}
		return left;
	}

	// Just take it out of the doubly linked list.
	private Node removeFromSibling(Node item) {
		if (item.parent != null) {
			if (item.parent.degree == 1) {
				item.parent.child = null;
			} else {
				item.parent.child = item.next;
			}
			item.parent.degree--; // reduce degree.
			item.parent = null;
		}
		item.next.prev = item.prev;
		item.prev.next = item.next;
		item.next = item;
		item.prev = item;
		return item;
	}

	// pass parent of current node as Item to do cascade cut. This sets the item
	// as true if false and does not cascade.
	private void cascadeCut(Node item) {
		Node tmp = item;
		while (tmp.parent != null && tmp.marked) {
			tmp.marked = false;
			Node t = tmp;
			tmp = tmp.parent;
			t = removeFromSibling(t);
			min = mergeLists(min, t);
			min = updateMin(min, t);
		}
		tmp.marked = true;
	}

	// add item as a child of root.
	private Node addChild(Node root, Node item) {
		root.child = mergeLists(root.child, item);
		root.degree++;
		item.parent = root;
		return root;
	}

	public void print() {
		print(min);
	}

	public void print(Node item) {
		if (item != null) {
			Node tmp = item;
			while (tmp.next != item) {
				System.out.println(tmp.data.getVerticeNo() + "    "
						+ tmp.data.getKey() + " " + tmp.degree + " "
						+ tmp.child);
				print(tmp.child);
				tmp = tmp.next;
			}
			System.out.println("**" + tmp.data.getVerticeNo() + "    "
					+ tmp.data.getKey() + " " + tmp.degree + " " + tmp.child);
			print(tmp.child);
		}
	}
}