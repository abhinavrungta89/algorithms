package ads;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Pair {
	String key;
	int value;

	public Pair(String ip, int hop) {
		key = ip;
		value = hop;
	}
}

class RouterItem {
	String ip;
	BinaryTrie bt;

	public RouterItem(String ip) {
		bt = new BinaryTrie();
		this.ip = ip;
	}
}

public class Routing {
	ArrayList<RouterItem> routerList = null;
	Graph g;
	String[] ipString;
	int n;

	public Routing(Graph nw) {
		g = nw;
		n = g.getNoOfNodes();
		routerList = new ArrayList<RouterItem>(n);
	}

	public static void main(String[] args) throws IOException {
		// to check if all the arguments are provided.
		if (args.length != 4) {
			System.out.println("Incorrect # of Arguments.");
			System.exit(0);
		}
		// get the list of arguments.
		String fileName = args[0];
		String ipListfileName = args[1];
		int source = Integer.parseInt(args[2]);
		int destination = Integer.parseInt(args[3]);

		Graph g = new Graph();
		g.loadGraph(fileName);
		Routing r = new Routing(g);
		r.loadIPs(ipListfileName);
		r.loadRouters();
		r.trimRouterTables();
		r.RouterPath(source, destination);
	}

	public void loadIPs(String ipListfileName) throws IOException {
		ipString = null;
		ipString = new String[n];
		// Read the input.
		String[] params = null;
		FileInputStream fstream = null;
		BufferedReader br = null;
		try {
			fstream = new FileInputStream(ipListfileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		br = new BufferedReader(new InputStreamReader(fstream));
		// Read the input and populate the adjacency list.
		for (int i = 0; i < n; i++) {
			String line = br.readLine();
			if (line.equalsIgnoreCase("")) {
				line = br.readLine();
			}
			params = line.split("\\.");
			String ipStr = "";
			for (int j = 0; j < 4; j++) {
				Byte b = (byte) Integer.parseInt(params[j]);
				ipStr += String.format("%8s", Integer.toBinaryString(b & 0xFF))
						.replace(' ', '0');
			}
			ipString[i] = ipStr;
		}
		br.close();
	}

	public void loadRouters() {
		// for all routers, call ssp, get the nextHop for all destination nodes.
		for (int i = 0; i < n; i++) {
			RouterItem item = new RouterItem(ipString[i]);
			routerList.add(i, item);
			// ssp for all destinations from node no. i
			g.SSP(i);
			for (int j = 0; j < n; j++) {
				if (i != j) {
					// get next hop for all except i;
					Pair ob = new Pair(ipString[j], g.getNextHop(i, j));
					// add the <ip, hop> pair to the binary tree of
					// corresponding router.
					item.bt.insert(ob);
				}
			}
		}
	}

	public void trimRouterTables() {
		for (int i = 0; i < n; i++) {
			routerList.get(i).bt.trim();
		}
	}

	public void RouterPath(int source, int destination) {
		g.SSP(source);
		g.printPathLength(destination);
		String destinationIP = ipString[destination];
		// get source router.
		RouterItem tmp = routerList.get(source);
		Pair p;
		do {
			p = tmp.bt.getNextHop(destinationIP);
			System.out.print(p.key + " ");
			tmp = routerList.get(p.value);
		} while (p.value != destination);
		System.out.println();
	}
}
