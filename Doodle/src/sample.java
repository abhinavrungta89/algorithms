import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.TreeMap;

class Node {
	int key, val;

	public Node(int key, int val) {
		this.key = key;
		this.val = val;
	}

	boolean equals(Node o) {
		if (this.key == o.key)
			return true;
		return false;
	}
}

class NodeComparator implements Comparator<Node> {

	public int compare(Node o1, Node o2) {
		if (o1.val > o2.val)
			return -1;
		else if (o1.val < o2.val)
			return 1;
		else {
			if (o1.key < o2.key)
				return -1;
			else
				return 1;
		}
	}
}

public class sample {
	public ArrayList<Integer> getMode(ArrayList<Integer> a, ArrayList<ArrayList<Integer>> b) {
		ArrayList<Integer> mode = new ArrayList<Integer>(b.size());
		HashMap<Integer, Node> freq = new HashMap<Integer, Node>();
		for (Integer item : a) {
			Node tmp = new Node(item, 1);
			if (freq.containsKey(item))
				freq.get(item).val++;
			else
				freq.put(item, tmp);
		}

		ArrayList<Node> pq = new ArrayList<Node>(freq.size());
		Iterator<Entry<Integer, Node>> itr = freq.entrySet().iterator();
		while (itr.hasNext()) {
			Entry<Integer, Node> item = itr.next();
			pq.add(item.getValue());
		}
		NodeComparator nc = new NodeComparator();

		for (int i = 0; i < b.size(); i++) {
			int key = b.get(i).get(0) - 1;
			int val = b.get(i).get(1);
			Node tmp = freq.get(a.get(key));
			if (tmp.val == 1) {
				pq.remove(tmp);
				freq.remove(a.get(key));
			} else
				tmp.val--;

			a.set(key, val);
			if (freq.containsKey(val))
				freq.get(val).val++;
			else {
				Node t2 = new Node(val, 1);
				freq.put(val, t2);
				pq.add(t2);
			}
			Collections.sort(pq, nc);
			mode.add(pq.get(0).key);
		}
		return mode;
	}

	public static void main(String[] args) {
		Queue bfs1 = new LinkedList();
		HashMap tm = new HashMap<Integer, Integer>();
	}
}
