package codecon.bloomberg;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class PopularGator {
	public static void main(String[] args) {
		HashMap<String, LinkedList<String>> edges = new HashMap<String, LinkedList<String>>();

		Scanner stdin = new Scanner(System.in);
		int n = Integer.parseInt(stdin.nextLine());
		for (int i = 0; i < n; i++) {
			String[] rel = stdin.nextLine().split(",");
			if (edges.containsKey(rel[0])) {
				edges.get(rel[0]).add(rel[1]);
			} else {
				LinkedList<String> friends = new LinkedList<String>();
				friends.add(rel[1]);
				edges.put(rel[0], friends);
			}

			if (edges.containsKey(rel[1])) {
				edges.get(rel[1]).add(rel[0]);
			} else {
				LinkedList<String> friends = new LinkedList<String>();
				friends.add(rel[0]);
				edges.put(rel[1], friends);
			}
		}
		String name = stdin.nextLine();
		stdin.close();
		LinkedList<String> list = edges.get(name);
		int max = Integer.MIN_VALUE;
		String popularGator = "";
		for (int i = 0; i < list.size(); i++) {
			int size = edges.get(list.get(i)).size();
			if (size > max) {
				max = size;
				popularGator = list.get(i);
				System.out.println(popularGator + " " + size);
			}
		}
		System.out.println(popularGator);
	}
}