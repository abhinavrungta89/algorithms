package codecon.bloomberg;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GigaBall {
	public static void main(String[] args) {
		HashMap<Integer, Integer> maxsum = new HashMap<Integer, Integer>();

		// scan input.
		Scanner stdin = new Scanner(System.in);
		String[] sizes = stdin.nextLine().split(" ");
		int l = Integer.parseInt(sizes[0]);
		int m = Integer.parseInt(sizes[1]);
		int n = Integer.parseInt(sizes[2]);

		int[] lar = new int[l];
		int[] mar = new int[m];
		int[] nar = new int[n];

		for (int i = 0; i < l; i++) {
			lar[i] = Integer.parseInt(stdin.nextLine());
		}

		for (int i = 0; i < m; i++) {
			mar[i] = Integer.parseInt(stdin.nextLine());
		}

		for (int i = 0; i < n; i++) {
			nar[i] = Integer.parseInt(stdin.nextLine());
		}

		for (int i = 0; i < l; i++) {
			for (int j = 0; j < m; j++) {
				for (int k = 0; k < n; k++) {
					int sum = lar[i] + mar[j] + nar[k];
					if (maxsum.containsKey(sum)) {
						int tmp = maxsum.get(sum);
						tmp += 1;
						maxsum.put(sum, tmp);
					} else {
						maxsum.put(sum, 1);
					}
				}
			}
		}
		int max = Integer.MIN_VALUE;
		int number = 0;
		for (Map.Entry<Integer, Integer> itr : maxsum.entrySet()) {
			if (itr.getValue() > max) {
				number = itr.getKey();
				max = itr.getValue();
			}
		}
		System.out.println(number);
		System.out.println(max);
		stdin.close();
	}
}