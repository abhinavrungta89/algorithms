package codecon.bloomberg;

import java.util.Arrays;
import java.util.Scanner;

public class Photo {
	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int width = Integer.parseInt(stdin.nextLine());
		int num = Integer.parseInt(stdin.nextLine());
		double[] arr = new double[num];
		for (int i = 0; i < num; i++) {
			arr[i] = Double.parseDouble(stdin.nextLine());
		}
		Arrays.sort(arr);
		int pos = 0;
		int max = 0;
		for (int i = 0; i < num; i++) {
			int tmp = 0;
			for (int j = i; j < num; j++) {
				if (arr[j] - arr[i] <= width) {
					tmp += 1;
				} else {
					break;
				}
			}
			if (max < tmp) {
				max = tmp;
				pos = i;
			}
		}
		System.out.println(arr[pos]);
		stdin.close();

	}

}