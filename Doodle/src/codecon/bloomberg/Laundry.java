package codecon.bloomberg;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Laundry {
	public static void main(String[] args) {

		HashMap<String, Integer> clothes = new HashMap<String, Integer>();
		TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(
				clothes);

		Scanner stdin = new Scanner(System.in);
		while (stdin.hasNextLine()) {
			String s = stdin.nextLine();
			if (s.equalsIgnoreCase("quit")) {
				break;
			}
			if (clothes.containsKey(s)) {
				int a = clothes.get(s);
				a += 1;
				clothes.put(s, a);
			} else {
				clothes.put(s, 1);
			}
		}
		stdin.close();
		sorted_map.putAll(clothes);
		for (Map.Entry<String, Integer> d : sorted_map.entrySet()) {
			if (d.getKey().endsWith("sock")) {
				int r = d.getValue() % 2;
				int q = d.getValue() / 2;
				if (q > 0) {
					System.out.println(q + "|" + d.getKey());
				}
				if (r == 1) {
					System.out.println("0|" + d.getKey());
				}
			} else {
				System.out.println(d.getValue() + "|" + d.getKey());
			}
		}
	}
}