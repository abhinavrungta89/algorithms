package codecon.bloomberg;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ComplianceExtended {
	static Set<String> palindromeSet = new HashSet<String>();

	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		String str = stdin.nextLine();
		System.out.println(noOfPalindromePermutations(str));

		stdin.close();
	}

	public static int noOfPalindromePermutations(String str) {
		palindromeSet.clear();
		if (checkAnagramisPalindrome(str.toCharArray())) {
			permutation("", str);
		}
		return palindromeSet.size();
	}

	private static void permutation(String prefix, String str) {
		int n = str.length();
		if (n == 0) {
			if (chkIfPalindrome(prefix))
				palindromeSet.add(prefix);
		} else {
			for (int i = 0; i < n; i++) {
				permutation(prefix + str.charAt(i),
						str.substring(0, i) + str.substring(i + 1, n));
			}
		}
	}

	public static boolean chkIfPalindrome(String s) {
		int length = s.length();
		int halfLength = length / 2;
		for (int i = 0; i < halfLength; i++) {
			if (s.charAt(i) != s.charAt(length - i - 1)) {
				return false;
			}
		}
		return true;
	}

	public static boolean checkAnagramisPalindrome(char[] str) {
		boolean oddflag = false;
		char prev = str[0];
		int ctr = 1;
		for (int i = 1; i < str.length; i++) {
			if (str[i] == prev) {
				ctr++;
			} else {
				if (ctr % 2 != 0) {
					if (oddflag) {
						return false;
					}
					oddflag = true;
				}
				ctr = 1;
				prev = str[i];
			}
		}
		if (oddflag && ctr % 2 != 0) {
			return false;
		}
		return true;
	}
}