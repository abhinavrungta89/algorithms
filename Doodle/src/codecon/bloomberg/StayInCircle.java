package codecon.bloomberg;

import java.util.Scanner;

public class StayInCircle {
	public static int getNextIndex(boolean[] removed, int start, int k, int n) {
		while (k > 0) {
			if (!removed[start])
				k--;
			if (k == 0)
				removed[start] = true;
			start += 1;
			start %= n;
		}
		return start;
	}

	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int n = Integer.parseInt(stdin.nextLine());
		int k = Integer.parseInt(stdin.nextLine());
		boolean[] removed = new boolean[n];
		int index = 0;
		int ctr = 1;
		while (ctr < n) {
			index = getNextIndex(removed, index, k, n);
			ctr++;
		}
		ctr = 1;
		for (boolean itr : removed) {
			if (!itr) {
				System.out.println(ctr);
				break;
			}
			ctr++;
		}
		stdin.close();

	}
}