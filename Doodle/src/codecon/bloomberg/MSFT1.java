package codecon.bloomberg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MSFT1 {
	public static void main(String[] args) {
		HashMap<Character, Integer> letterCount = new HashMap<Character, Integer>();
		// scan input.
		Scanner stdin = new Scanner(System.in);
		ArrayList<Integer> output_x = new ArrayList<Integer>();
		ArrayList<Integer> output_y = new ArrayList<Integer>();
		int odd_count;
		int x, y;
		while (stdin.hasNext()) {
			x = 0;
			y = 1;
			String word = stdin.nextLine();
			if (word.equalsIgnoreCase("exit")) {
				break;
			}
			char[] arr = word.toCharArray();

			for (int i = 0; i < arr.length; i++) {
				char c = arr[i];
				if (letterCount.containsKey(c)) {
					int count = letterCount.get(c);
					count += 1;
					letterCount.put(c, count);
				} else {
					letterCount.put(c, 1);
				}
			}
			while (true) {
				odd_count = 0;
				char min_oddchar = ' ';
				int min_oddcharcount = Integer.MAX_VALUE;
				for (Map.Entry<Character, Integer> entry : letterCount.entrySet()) {
					char key = entry.getKey();
					int value = entry.getValue();
					// System.out.println(key);
					// System.out.println(value);
					if (value % 2 == 1) {
						odd_count++;
						if (value < min_oddcharcount) {
							min_oddcharcount = value;
							min_oddchar = key;
						}
					}
				}
				// System.out.println("O" + odd_count);
				if (odd_count > 1) {
					x++;
					// System.out.println("Min char " + min_oddchar);
					// System.out.println("Min char ct" + min_oddcharcount);
					// need to remove letters.
					if (min_oddcharcount == 1) {
						letterCount.remove(min_oddchar);
					} else {
						letterCount.put(min_oddchar, min_oddcharcount - 1);
					}

				} else if (odd_count == 0 || odd_count == 1) {
					break;
				}
			}
			int n = 0;
			int fact = 1;
			for (Map.Entry<Character, Integer> entry : letterCount.entrySet()) {
				int value = entry.getValue() / 2;
				n += value;
				for (int j = 1; j <= value; j++)
					fact = fact * j;
			}
			for (int j = 1; j <= n; j++)
				y = y * j;
			y = y / fact;
			output_x.add(x);
			output_y.add(y);
			// System.out.println(x + "," + y);
			letterCount.clear();
		}
		stdin.close();
		for (int k = 0; k < output_x.size(); k++) {
			System.out.println(output_x.get(k) + "," + output_y.get(k));
		}
	}
}