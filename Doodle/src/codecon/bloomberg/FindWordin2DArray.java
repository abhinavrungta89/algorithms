package codecon.bloomberg;

import java.util.ArrayList;
import java.util.Scanner;

public class FindWordin2DArray {

	int x[] = { 1, -1, 0, 0 };
	int y[] = { 0, 0, 1, -1 };

	public boolean isSafe(char[][] board, int r, int c, String txt, int ind,
			boolean[][] usedArr) {
		if (r >= 0 && r < board.length && c >= 0 && c < board[0].length
				&& board[r][c] == txt.charAt(ind) && !usedArr[r][c]) {
			usedArr[r][c] = true;
			return true;
		}

		return false;
	}

	public boolean findneighbour(char[][] board, String str, int index,
			int row, int col, boolean[][] usedArray) {
		if (index == str.length())
			return true;

		if (isSafe(board, row, col, str, index, usedArray)) {
			for (int n = 0; n < 4; n++) {
				if (findneighbour(board, str, index + 1, row + x[n],
						col + y[n], usedArray)) {
					return true;
				}
			}
			usedArray[row][col] = false;

			return false;
		}
		return false;
	}

	public static void main(String args[]) {
		Scanner stdin = new Scanner(System.in);
		String str = stdin.nextLine();
		ArrayList<String> tmpList = new ArrayList<String>();
		while (stdin.hasNextLine()) {
			String h = stdin.nextLine();
			if (h.equalsIgnoreCase("quit")) {
				break;
			} else {
				tmpList.add(h);
			}

		}
		stdin.close();
		char[][] board = new char[tmpList.size()][tmpList.get(0).length()];

		for (int i = 0; i < tmpList.size(); i++) {
			String tmp = tmpList.get(i);
			for (int k = 0; k < tmp.length(); k++) {
				board[i][k] = tmp.charAt(k);
			}
		}
		boolean[][] used = new boolean[board.length][board[0].length];
		for (int i = 0; i < board.length; i++) {
			for (int k = 0; k < board[0].length; k++) {
				used[i][k] = false;
			}
		}
		FindWordin2DArray sol = new FindWordin2DArray();
		if (sol.findneighbour(board, str, 0, 0, 0, used)) {
			for (int i = 0; i < board.length; i++) {
				for (int k = 0; k < board[0].length; k++) {
					if (!used[i][k]) {
						board[i][k] = '.';
					}
				}
			}
			for (int i = 0; i < board.length; i++) {
				System.out.println(board[i]);
			}
		}
	}
}