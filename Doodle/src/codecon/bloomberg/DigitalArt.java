package codecon.bloomberg;

import java.util.Scanner;

public class DigitalArt {

	public static int largestSquare(int[][] ar, int i, int j) {
		int number = ar[i][j];
		int r = i;
		int c = j;
		int n = ar.length;
		int m = ar[0].length;
		int length = 0;
		// start checking for number to the right and bottom.
		while (r < n && c < m && ar[r][j] == number && ar[i][c] == number) {
			r++;
			c++;
		}
		length = r - i;
		// if possible square is of size atleast 2*2
		if (r - i > 1) {
			r -= 1;
			c -= 1;
			while (i <= r && j <= c && ar[r][j] == number && ar[i][c] == number) {
				i++;
				j++;
			}
			// square found
			if (i > r) {
				return length;
			}
		}
		return 0;
	}

	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int m = Integer.parseInt(stdin.nextLine());
		int n = Integer.parseInt(stdin.nextLine());
		int[][] arr = new int[n][m];
		for (int i = 0; i < n; i++) {
			String inp = stdin.nextLine();
			for (int k = 0; k < m; k++) {
				arr[i][k] = inp.charAt(k) - '0';
			}
		}
		stdin.close();

		int max = Integer.MIN_VALUE;
		int posi = 0, posj = 0;
		for (int i = 0; i < n; i++) {
			for (int k = 0; k < m; k++) {
				int tmp = largestSquare(arr, i, k);
				if (tmp != 0) {
					if (tmp > max) {
						max = tmp;
						posi = i;
						posj = k;
					}
				}
			}
		}

		System.out.println(arr[posi][posj]);
		System.out.println(posi + "," + posj);
		System.out.println((posi + max - 1) + "," + (posj + max - 1));
	}
}