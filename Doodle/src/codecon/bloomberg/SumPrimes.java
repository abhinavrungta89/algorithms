package codecon.bloomberg;

//Problem        : Sum Those Primes
//Language       : Java
//Compiled Using : javac
//Version        : Java 1.7.0_65
//Input for your program will be provided from STDIN
//Print out all output from your program to STDOUT

import java.util.Scanner;

//Your submission should *ONLY* use the following class name
public class SumPrimes {
	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int a = Integer.parseInt(stdin.nextLine());
		int b = Integer.parseInt(stdin.nextLine());
		int sum = 0;
		while (a <= b) {
			if (isPrime(a)) {
				// System.out.println(a);
				sum += a;
			}
			a += 1;
		}
		stdin.close();
		System.out.println(sum);
	}

	static boolean isPrime(int n) {
		for (int i = 2; i < n; i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}
}