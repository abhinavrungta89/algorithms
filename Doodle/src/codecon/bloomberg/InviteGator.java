package codecon.bloomberg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class InviteGator {
	public static void main(String[] args) {
		HashMap<String, Set<String>> edges = new HashMap<String, Set<String>>();

		Scanner stdin = new Scanner(System.in);
		int n = Integer.parseInt(stdin.nextLine());
		for (int i = 0; i < n; i++) {
			String[] rel = stdin.nextLine().split(",");
			if (edges.containsKey(rel[0])) {
				edges.get(rel[0]).add(rel[1]);
			} else {
				Set<String> friends = new HashSet<String>();
				friends.add(rel[1]);
				edges.put(rel[0], friends);
			}

			if (edges.containsKey(rel[1])) {
				edges.get(rel[1]).add(rel[0]);
			} else {
				Set<String> friends = new HashSet<String>();
				friends.add(rel[0]);
				edges.put(rel[1], friends);
			}
		}
		String name = stdin.nextLine();
		stdin.close();
		Set<String> self = edges.get(name);
		Iterator<String> itr = self.iterator();
		int max = Integer.MIN_VALUE;
		String popularGator = "";
		while (itr.hasNext()) {
			String tmp = itr.next();
			Set<String> tmpSet = new HashSet<String>();
			tmpSet.addAll(edges.get(tmp));
			tmpSet.removeAll(self);
			tmpSet.remove(name);
			int size = tmpSet.size();
			if (size > max) {
				max = size;
				popularGator = tmp;
			}
		}
		System.out.println(popularGator);
	}
}