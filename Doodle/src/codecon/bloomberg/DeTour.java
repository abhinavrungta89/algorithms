package codecon.bloomberg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GeoLocation {
	private double latitude;
	private double longitude;

	public GeoLocation(double i, double j) {
		latitude = i;
		longitude = j;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double x) {
		this.latitude = x;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double y) {
		this.longitude = y;
	}
}

public class DeTour {
	static Map<String, GeoLocation> GeoLocations = new HashMap<String, GeoLocation>();

	public static void main(String[] args) {
		// Initialize Locations
		GeoLocations.put("A", new GeoLocation(0, 0));
		GeoLocations.put("B", new GeoLocation(25, 100));
		GeoLocations.put("C", new GeoLocation(10, 35));
		GeoLocations.put("D", new GeoLocation(0, 25));

		List<Double> distances = new ArrayList<Double>();
		// Set possible Paths
		List<String> paths = new ArrayList<String>();
		paths.add("ACDB");
		paths.add("CABD");

		// Calculate Distances
		for (String path : paths) {
			char[] stops = path.toCharArray();
			double distance = 0.0;
			for (int index = 0; index < stops.length - 1; index++) {
				distance += calculatedistance(GeoLocations.get("" + stops[index]),
						GeoLocations.get("" + stops[index + 1]));
			}
			distances.add(distance);
		}

		// Select Minimum Distance
		double leastDistance = Collections.min(distances);
		String bestPath = paths.get(distances.indexOf(leastDistance));
		System.out.println("Shortest Detour is " + bestPath + " with a distance of " + leastDistance);
	}

	/**
	 * Calculate distance between two Points
	 * 
	 * @param start
	 * @param end
	 * @return distance
	 */
	public static double calculatedistance(GeoLocation start, GeoLocation end) {
		double Radius = 6371000;
		double endLatitude = end.getLatitude();
		double startLatitude = start.getLatitude();
		double dLatitude = Math.toRadians(endLatitude - startLatitude);
		double endLongitude = end.getLongitude();
		double startLongitude = start.getLongitude();
		double dLongitude = Math.toRadians(endLongitude - startLongitude);

		double a = Math.sin(dLatitude / 2) * Math.sin(dLatitude / 2) + Math.cos(Math.toRadians(startLatitude))
				* Math.cos(Math.toRadians(endLatitude)) * Math.sin(dLongitude / 2) * Math.sin(dLongitude / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return Radius * c;
	}
}