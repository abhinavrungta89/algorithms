package codecon.bloomberg;

import java.util.Arrays;
import java.util.Scanner;

public class Compliance {
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		char[] str = stdin.nextLine().toCharArray();
		Arrays.sort(str);
		if (checkAnagramisPalindrome(str)) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
		stdin.close();
	}

	public static boolean checkAnagramisPalindrome(char[] str) {
		boolean oddflag = false;
		char prev = str[0];
		int ctr = 1;
		for (int i = 1; i < str.length; i++) {
			if (str[i] == prev) {
				ctr++;
			} else {
				if (ctr % 2 != 0) {
					if (oddflag) {
						return false;
					}
					oddflag = true;
				}
				ctr = 1;
				prev = str[i];
			}
		}
		if (oddflag && ctr % 2 != 0) {
			return false;
		}
		return true;
	}
}