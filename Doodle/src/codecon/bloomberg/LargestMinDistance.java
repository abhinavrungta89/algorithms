package codecon.bloomberg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

//Your submission should *ONLY* use the following class name
public class LargestMinDistance {
	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		String input = stdin.nextLine();
		String ar[] = input.split(" ");
		int n = Integer.parseInt(ar[0]);
		int s = Integer.parseInt(ar[1]);
		ArrayList<Integer> inputpos = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			inputpos.add(Integer.parseInt(stdin.nextLine()));
		}
		stdin.close();
		Collections.sort(inputpos);
		System.out.println(solve(inputpos, s));
	}

	public static boolean isFeasible(ArrayList<Integer> positions, int dist,
			int k) {
		int taken = 1;
		int last = positions.get(0);
		for (int i = 1; i < positions.size(); i++) {
			if (positions.get(i) - last >= dist) {
				taken++;
				last = positions.get(i);
			}
		}
		return taken >= k;
	}

	public static int solve(ArrayList<Integer> positions, int k) {
		int low = 0; // definitely small enough
		int high = Collections.max(positions) - Collections.min(positions) + 1;// definitely
																				// too
																				// big.
		while (high - low > 1) {
			int mid = (low + high) / 2;
			if (isFeasible(positions, mid, k)) {
				low = mid;
			} else {
				high = mid;
			}
		}
		return low;
	}
}