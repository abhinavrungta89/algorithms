package codecon.bloomberg;

import java.util.Scanner;

//Your submission should *ONLY* use the following class name
public class SecretMessage {
	static String[][] arr = {};

	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int num = Integer.parseInt(stdin.nextLine());
		decode(num, stdin);
		stdin.close();

	}

	public static void decode(int num, Scanner stdin) {
		arr = new String[num][];
		int i = 0;
		while (stdin.hasNextLine()) {
			arr[i] = stdin.nextLine().split(" ");
			i++;
		}
		printDecoded(arr);
	}

	public static void printDecoded(String[][] arr) {
		String s = arr[0][0];
		int oldi = 0;
		int oldj = 0;
		boolean end = false;
		while (!end) {
			boolean unique = true;
			// System.out.println(s);
			for_loop: for (int k = 0; k < arr.length; k++) {
				for (int l = 0; l < arr[k].length; l++) {
					// System.out.println(arr[k][l] + k + " " + l);
					if (s.equalsIgnoreCase(arr[k][l])
							&& (k != oldi || l != oldj)) {
						System.out.print(arr[k][l] + " ");
						oldi = k;
						oldj = l + 1;
						s = arr[oldi][oldj];
						unique = false;
						break for_loop;
					}
				}
			}
			if (unique) {
				if (oldj != arr[oldi].length - 1) {
					oldj = oldj + 1;
					s = arr[oldi][oldj];
				} else {
					end = true;
					System.out.println(arr[oldi][oldj]);
				}
			}

		}
	}
}