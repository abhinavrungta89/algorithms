package codecon.bloomberg;

//Problem        : World Cup
//Language       : Java
//Compiled Using : javac
//Version        : Java 1.7.0_65
//Input for your program will be provided from STDIN
//Print out all output from your program to STDOUT

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

class Details {
	public int wins = 0;
	public int goalwins = 0;

}

// Constructs a map of no of wins and gaol wins for each team. Uses a comparator
// to rank.
public class WorldCup {
	public static void main(String[] args) {

		HashMap<String, Details> wins = new HashMap<String, Details>();
		ValueComparator bvc = new ValueComparator(wins);
		TreeMap<String, Details> sorted_map = new TreeMap<String, Details>(bvc);

		Scanner stdin = new Scanner(System.in);
		while (stdin.hasNextLine()) {
			String s = stdin.nextLine();
			if (s.equalsIgnoreCase("quit")) {
				break;
			}
			String[] result = s.split(" ");
			int bd = 0;
			for (int i = 1; i < result.length; i++) {
				if (result[i].contains("-")) {
					bd = i;
					break;
				}
			}
			for (int i = 1; i < bd; i++) {
				result[0] += " " + result[i];
			}
			result[1] = result[bd];
			result[2] = result[bd + 1];
			for (int i = bd + 2; i < result.length; i++) {
				result[2] += " " + result[i];
			}

			String[] count = result[1].split("-");
			int l = Integer.parseInt(count[0]);
			int r = Integer.parseInt(count[1]);
			int lpts = 0;
			int rpts = 0;
			if (l > r) {
				lpts = 3;
				rpts = 0;
			} else if (l < r) {
				lpts = 0;
				rpts = 3;
			} else if (l == r) {
				lpts = 1;
				rpts = 1;
			}
			int lgoaldiff = l - r;
			int rgoaldiff = r - l;

			if (wins.containsKey(result[2])) {
				wins.get(result[2]).wins += rpts;
				wins.get(result[2]).goalwins += rgoaldiff;
			} else {
				Details ob = new Details();
				ob.wins = rpts;
				ob.goalwins = rgoaldiff;
				wins.put(result[2], ob);
			}

			if (wins.containsKey(result[0])) {
				wins.get(result[0]).wins += lpts;
				wins.get(result[0]).goalwins += lgoaldiff;
			} else {
				Details ob = new Details();
				ob.wins = lpts;
				ob.goalwins = lgoaldiff;
				wins.put(result[0], ob);
			}
		}
		stdin.close();
		sorted_map.putAll(wins);

		Iterator<String> itr = sorted_map.keySet().iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}
}

class ValueComparator implements Comparator<String> {

	HashMap<String, Details> base;

	public ValueComparator(HashMap<String, Details> base) {
		this.base = base;
	}

	// Note: this comparator imposes orderings that are inconsistent with
	// equals.
	public int compare(String a, String b) {
		if (base.get(a).wins > base.get(b).wins) {
			return -1;
		} else if (base.get(a).wins < base.get(b).wins) {
			return 1;
		} else {
			if (base.get(a).goalwins >= base.get(b).goalwins) {
				return -1;
			} else {
				return 1;
			}
		}
	}
}
