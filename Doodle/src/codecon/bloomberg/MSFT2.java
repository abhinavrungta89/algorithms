package codecon.bloomberg;

import java.util.Scanner;

public class MSFT2 {
	public static void main(String[] args) {
		// scan input.
		Scanner stdin = new Scanner(System.in);
		while (stdin.hasNext()) {
			String word = stdin.nextLine();
			if (word.equalsIgnoreCase("exit")) {
				break;
			}

			String[] arr = word.split(",");
			int orig_base = Integer.parseInt(arr[1]);
			int new_base = Integer.parseInt(arr[2]);
			if (orig_base < 2 || orig_base > 36 || new_base < 2 || new_base > 36 || !arr[0].matches("[a-z0-9]+")) {
				System.out.println("Invalid Input");
				continue;
			}
			System.out.println(Integer.toString(Integer.parseInt(arr[0], orig_base), new_base));
		}
		stdin.close();
	}
}