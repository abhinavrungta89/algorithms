package codecon.bloomberg;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class BuildBinaryTree {

	// Definition for binary tree
	public class Node {
		int val;
		Node left;
		Node right;

		Node(int x) {
			val = x;
		}
	}

	public Node buildTree(int[] inorder, int[] postorder) {
		return buildTree(inorder, 0, inorder.length - 1, postorder, 0,
				postorder.length - 1);
	}

	public Node buildTree(int[] inorder, int inStart, int inEnd,
			int[] postorder, int postStart, int postEnd) {
		if (inStart > inEnd || postStart > postEnd)
			return null;

		int rootValue = postorder[postEnd];
		Node root = new Node(rootValue);

		int k = 0;
		for (int i = 0; i < inorder.length; i++) {
			if (inorder[i] == rootValue) {
				k = i;
				break;
			}
		}
		root.left = buildTree(inorder, inStart, k - 1, postorder, postStart,
				postStart + k - (inStart + 1));
		root.right = buildTree(inorder, k + 1, inEnd, postorder, postStart + k
				- inStart, postEnd - 1);

		return root;
	}

	Queue<Node> queue = new LinkedList<Node>();

	public void BFS(Node root) {
		if (root == null)
			return;
		queue.clear();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node node = queue.remove();
			if (node != root) {
				System.out.print("," + node.val);
			} else {
				System.out.print(node.val);
			}
			if (node.left != null)
				queue.add(node.left);
			if (node.right != null)
				queue.add(node.right);
		}

	}

	public static void main(String args[]) throws Exception {
		Scanner stdin = new Scanner(System.in);
		String[] strin = stdin.nextLine().split(",");
		String[] strpost = stdin.nextLine().split(",");
		try {
			int[] inorder = new int[strin.length];
			int[] postorder = new int[strin.length];
			for (int i = 0; i < strin.length; i++) {
				inorder[i] = Integer.parseInt(strin[i]);
				postorder[i] = Integer.parseInt(strpost[i]);
			}

			BuildBinaryTree ob = new BuildBinaryTree();
			Node root = null;

			root = ob.buildTree(inorder, postorder);
			ob.BFS(root);
		} catch (StackOverflowError e) {
			System.out.println("InvalidInput");
		} catch (Exception e) {
			System.out.println("InvalidInput");
		}
		stdin.close();
	}
}