package codecon.bloomberg;

import java.util.ArrayList;
import java.util.Scanner;

public class New {

	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		MaxSubArray(stdin);

	}

	public static void NonDecreasingSequence(Scanner stdin) {
		int tests = Integer.parseInt(stdin.nextLine());
		while (tests > 0) {
			String[] tmp = stdin.nextLine().split(" ");
			int n = Integer.parseInt(tmp[0]);
			int k = Integer.parseInt(tmp[1]);
			ArrayList<Integer> l = new ArrayList<Integer>();
			System.out.println(NonDecRecur(l, n, k));
			tests--;
		}
	}

	public static int NonDecRecur(ArrayList<Integer> l, int n, int k) {
		if (l.size() == k) {
			if (gcdList(l) == 1)
				return 1;
			else
				return 0;
		} else {
			int count = 0;
			int curr;
			if (l.size() == 0) {
				curr = 1;
			} else {
				curr = l.get(l.size() - 1);
			}
			for (int i = curr; i <= n; i++) {
				l.add(i);
				count += NonDecRecur(l, n, k);
				l.remove(Integer.valueOf(i));
			}
			return count;
		}
	}

	public static int gcdList(ArrayList<Integer> l) {
		int result = l.get(0);
		for (int i = 1; i < l.size(); i++) {
			result = gcd(result, l.get(i));
		}
		return result;
	}

	public static int gcd(int a, int b) {
		if (a == 0 || b == 0)
			return a + b; // base case
		return gcd(b, a % b);
	}

	public static void MaxSubArray(Scanner stdin) {
		int tests = Integer.parseInt(stdin.nextLine());
		while (tests > 0) {
			String[] tmp = stdin.nextLine().split(" ");
			long m = Long.parseLong(tmp[1]);
			int n = Integer.parseInt(tmp[0]);
			tmp = stdin.nextLine().split(" ");
			long countprev = Long.parseLong(tmp[0]) % m;
			long max = countprev;
			long count;
			for (int i = 1; i < n; i++) {
				long tmpInt = Long.parseLong(tmp[i]);
				count = Math.max(tmpInt % m, (countprev + tmpInt) % m);
				if (count > max) {
					max = count;
				}
				countprev = count;
			}
			System.out.println(max);
			tests--;
		}
	}

	public static void AngryProf(Scanner stdin) {
		int tests = Integer.parseInt(stdin.nextLine());
		while (tests > 0) {
			String[] tmp = stdin.nextLine().split(" ");
			int[] kn = new int[2];
			for (int i = 0; i < tmp.length; i++) {
				kn[i] = Integer.parseInt(tmp[i]);
			}
			tmp = stdin.nextLine().split(" ");
			int count = 0;
			for (int i = 0; i < kn[0]; i++) {
				if (Integer.parseInt(tmp[i]) <= 0)
					count++;
			}
			if (count < kn[1]) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
			tests--;
		}
	}
}