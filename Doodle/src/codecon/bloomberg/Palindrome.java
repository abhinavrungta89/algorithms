package codecon.bloomberg;

//Your submission should *ONLY* use the following class name
public class Palindrome {
	public static void main(String[] args) {
		System.out.println(newPal(args[0]));
	}

	public static int base10(String s) {
		int base_max = -1;
		int length = s.length();
		for (int i = 0; i < length; i++) {
			int tmp = s.charAt(i) - '0';
			if (tmp > 9) {
				tmp = s.charAt(i) - 'A';
				tmp += 10;
			}
			if (base_max < tmp) {
				base_max = tmp;
			}
		}
		base_max += 1;
		System.out.println(base_max);
		int value = 0;
		for (int i = 0; i < length; i++) {
			int tmp = s.charAt(i) - '0';
			if (tmp > 9) {
				tmp = s.charAt(i) - 'A';
				tmp += 10;
			}
			value += tmp * Math.pow(base_max, length - i - 1);
		}
		System.out.println(value);
		return value;
	}

	public static int newPal(String s) {
		int origLength = s.length();
		while (chkIfPalindrome(s) == -1 && s.length() > 1) {
			s = s.substring(0, s.length() - 1);
		}
		return origLength * 2 - s.length();
	}

	public static int chkIfPalindrome(String s) {
		int length = s.length();
		int halfLength = length / 2;
		for (int i = 0; i < halfLength; i++) {
			if (s.charAt(i) != s.charAt(length - i - 1)) {
				return -1;
			}
		}
		return length;
	}
}