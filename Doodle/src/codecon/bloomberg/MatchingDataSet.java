package codecon.bloomberg;

import java.util.ArrayList;
import java.util.Scanner;

//Your submission should *ONLY* use the following class name
public class MatchingDataSet {
	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int inpSize = Integer.parseInt(stdin.nextLine());
		ArrayList<ArrayList<Double>> oldSet = new ArrayList<ArrayList<Double>>(
				inpSize);
		ArrayList<ArrayList<Double>> newSet = new ArrayList<ArrayList<Double>>(
				inpSize);

		for (int i = 0; i < inpSize; i++) {
			String[] tmp = stdin.nextLine().split(",");
			ArrayList<Double> tmpList = new ArrayList<Double>();
			for (int k = 0; k < tmp.length; k++) {
				tmpList.add(Double.parseDouble(tmp[k]));
			}
			oldSet.add(i, tmpList);
		}

		for (int i = 0; i < inpSize; i++) {
			String[] tmp = stdin.nextLine().split(",");
			ArrayList<Double> tmpList = new ArrayList<Double>();
			for (int k = 0; k < tmp.length; k++) {
				tmpList.add(Double.parseDouble(tmp[k]));
			}
			newSet.add(i, tmpList);
		}
		stdin.close();
		for (int i = 0; i < inpSize; i++) {
			int k = getPairing(newSet, oldSet.get(i));
			System.out.println(i + "," + k);
			newSet.get(k).clear();
		}

	}

	public static int getPairing(ArrayList<ArrayList<Double>> newSet,
			ArrayList<Double> oldItem) {
		int min = Double.MAX_EXPONENT;
		int index = 0;
		int distance = 0;
		for (int i = 0; i < newSet.size(); i++) {
			ArrayList<Double> tmp = newSet.get(i);
			distance = 0;
			if (tmp.size() > 0) {
				// calculate distance
				for (int k = 0; k < tmp.size(); k++) {
					if (distance < min)
						distance += Math.abs(tmp.get(k) - oldItem.get(k));
					else
						break;
				}
				// select the best pair from old set with new item.
				if (distance < min) {
					index = i;
					min = distance;
				}
			}

		}
		return index;
	}
}