package codecon.bloomberg;

import java.util.Arrays;
import java.util.Scanner;

public class DNA {
	static String[] genomes = { "CCC", "TGT", "GGA", "ACA" };

	public static int getI(String s) {
		for (int i = 0; i < genomes.length; i++) {
			if (genomes[i].equalsIgnoreCase(s))
				return i;
		}
		return -1;
	}

	// return the longest common prefix of s and t
	public static String lcp(String s, String t) {
		int n = Math.min(s.length(), t.length());
		for (int i = 0; i < n; i++) {
			if (s.charAt(i) != t.charAt(i))
				return s.substring(0, i);
		}
		return s.substring(0, n);
	}

	// return the longest repeated string in s
	public static String lrs(String s) {

		// form the N suffixes
		int N = s.length();
		String[] suffixes = new String[N];
		for (int i = 0; i < N; i++) {
			suffixes[i] = s.substring(i, N);
		}

		// sort them
		Arrays.sort(suffixes);

		// find longest repeated substring by comparing adjacent sorted suffixes
		String lrs = "";
		for (int i = 0; i < N - 1; i++) {
			String x = lcp(suffixes[i], suffixes[i + 1]);
			if (x.length() > lrs.length())
				lrs = x;
		}
		return lrs;
	}

	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		String sequence = stdin.nextLine();
		stdin.close();

		String cstr = "";
		int i = 0;
		while (i < sequence.length()) {
			cstr += getI(sequence.substring(i, i + 3));
			i += 3;
		}
		String tmp = lrs(cstr);
		i = 0;
		while (i < tmp.length()) {
			System.out.print(genomes[(tmp.charAt(i) - '0')]);
			i++;
		}
	}
}