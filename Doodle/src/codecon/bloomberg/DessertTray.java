package codecon.bloomberg;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DessertTray {
	public static void main(String[] args) {

		HashMap<String, Integer> desserts = new HashMap<String, Integer>();
		TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(
				desserts);

		Scanner stdin = new Scanner(System.in);
		while (stdin.hasNextLine()) {
			String s = stdin.nextLine();
			if (s.equalsIgnoreCase("quit")) {
				break;
			}
			if (desserts.containsKey(s)) {
				int a = desserts.get(s);
				a += 1;
				if (s.endsWith("slice")) {
					if (a == 4) {
						desserts.put(s, a - 4);
						String tmp = s.substring(0,
								s.length() - "slice".length() - 1);
						if (desserts.containsKey(tmp)) {
							int b = desserts.get(tmp);
							b += 1;
							desserts.put(tmp, b);
						} else {
							desserts.put(tmp, 1);
						}
					} else {
						desserts.put(s, a);
					}
				} else {
					desserts.put(s, a);
				}
			} else {
				desserts.put(s, 1);
			}
		}
		stdin.close();
		sorted_map.putAll(desserts);
		for (Map.Entry<String, Integer> d : sorted_map.entrySet()) {
			System.out.println(d.getValue() + "|" + d.getKey());
		}
	}
}