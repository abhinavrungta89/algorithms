package codecon.bloomberg;

import java.util.Arrays;
import java.util.Scanner;

public class GoodKing {
	public static void main(String[] args) {

		Scanner stdin = new Scanner(System.in);
		int num = Integer.parseInt(stdin.nextLine());
		boolean[] state = new boolean[num];
		for (int i = 0; i < num; i++) {
			state[i] = false;
		}
		for (int i = 1; i <= num; i++) {
			for (int j = 1; j <= num; j++) {
				if (j % i == 0) {
					state[j - 1] = !state[j - 1];
				}
			}
		}
		System.out.println(Arrays.toString(state));
		stdin.close();

	}

}