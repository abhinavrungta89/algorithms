package codecon.bloomberg;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class RomanNumerals {

	public static String[] ConvertNumerals(int[] number) {
		String[] str = new String[number.length];
		LinkedHashMap<Integer, String> romans = new LinkedHashMap<Integer, String>();
		romans.put(1000, "M");
		romans.put(900, "CM");
		romans.put(500, "D");
		romans.put(400, "CD");
		romans.put(100, "C");
		romans.put(90, "XC");
		romans.put(50, "L");
		romans.put(40, "XL");
		romans.put(10, "X");
		romans.put(9, "IX");
		romans.put(5, "V");
		romans.put(4, "IV");
		romans.put(1, "I");

		for (int i = 0; i < number.length; i++) {
			String res = "";
			Iterator<Integer> itr = romans.keySet().iterator();
			while (itr.hasNext()) {
				int tmp = itr.next();
				System.out.println(tmp);
				int q = number[i] / tmp;
				while (q > 0) {
					res += romans.get(tmp);
					q--;
				}
				number[i] %= tmp;
			}
			str[i] = res;
		}
		return str;
	}

	public static String repeat(String s, int n) {
		if (s == null) {
			return null;
		}
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append(s);
		}
		return sb.toString();
	}

	public static void main(String args[]) throws Exception {
		System.out.println(ConvertNumerals(new int[6]));
	}
}
