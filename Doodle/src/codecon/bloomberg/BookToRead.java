package codecon.bloomberg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class BookToRead {
	public static void main(String[] args) {
		HashMap<String, Set<String>> edges = new HashMap<String, Set<String>>();
		HashMap<String, Set<String>> books = new HashMap<String, Set<String>>();

		// scan input.
		Scanner stdin = new Scanner(System.in);
		String name = stdin.nextLine();
		int d = Integer.parseInt(stdin.nextLine());
		int nedges = Integer.parseInt(stdin.nextLine());
		int nbooks = Integer.parseInt(stdin.nextLine());

		// add social graph to edges.
		for (int i = 0; i < nedges; i++) {
			String[] rel = stdin.nextLine().split("\\|");
			if (edges.containsKey(rel[0])) {
				edges.get(rel[0]).add(rel[1]);
			} else {
				Set<String> friends = new HashSet<String>();
				friends.add(rel[1]);
				edges.put(rel[0], friends);
			}

			if (edges.containsKey(rel[1])) {
				edges.get(rel[1]).add(rel[0]);
			} else {
				Set<String> friends = new HashSet<String>();
				friends.add(rel[0]);
				edges.put(rel[1], friends);
			}
		}
		// add books list
		for (int i = 0; i < nbooks; i++) {
			String[] booklist = stdin.nextLine().split("\\|");
			Set<String> list = new HashSet<String>();
			for (int k = 1; k < booklist.length; k++) {
				list.add(booklist[k]);
			}
			books.put(booklist[0], list);
		}
		stdin.close();

		Set<String> selfBooks = books.get(name);
		Set<String> tmpSet = new HashSet<String>();
		Queue<String> queue = new LinkedList<String>();
		Queue<String> temp = new LinkedList<String>();
		temp.clear();
		queue.clear();
		queue.add(name);
		int levelctr = 0;
		while (!queue.isEmpty()) {
			String node = queue.remove();
			tmpSet.addAll(books.get(node));
			tmpSet.removeAll(selfBooks);
			temp.addAll(edges.get(node));
			if (queue.isEmpty()) {
				levelctr++;
				if (levelctr > d) {
					break;
				}
				while (!temp.isEmpty()) {
					queue.add(temp.remove());
				}
			}
		}
		System.out.println(tmpSet.size());
	}
}